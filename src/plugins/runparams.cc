#include "wmi/plugins/runparams.h"
#include <ipc/partition.h>
#include <ipc/core.h>
#include <ipc/object.h>
#include <rc/RunParamsNamed.h>
#include <list>
#include <vector>
#include <iostream>
#include <sstream>
using namespace daq::wmi;
typedef std::list<IPCPartition> PLISTINT;

RunParamsPlugin::RunParamsPlugin()
{
}

RunParamsPlugin::~RunParamsPlugin()
{
    delete m_outStream;
}

std::string getLongString(long l)
{
    std::ostringstream out;
    out << l;
    return out.str();
}
void RunParamsPlugin::periodicAction()
{
    int tLen = 2;
    PLISTINT lst;
    std::list < std::pair<std::string, std::string> > opt;
    std::vector < std::string > pNames;
    IPCPartition::getPartitions(lst);
    if (lst.size() > 0)
    {
	tLen = tLen + 2 * lst.size();
	if (!pNames.empty())
	    pNames.clear();
	for (PLISTINT::iterator i = lst.begin(); i != lst.end(); ++i)
	{
	    IPCPartition p = *i;
	    pNames.push_back(p.name());
	}
    }
    m_outStream->open();
    std::ostringstream value;
    Text tx("Working partitions:");
    m_outStream->write(tx);
    BR br;
    m_outStream->write(br);
    for (unsigned int i = 0; i < pNames.size(); i++)
    {
	Link l(pNames[i] + ".html", pNames[i]);
	m_outStream->write(l);
	m_outStream->write(br);
    }
    m_outStream->close();
    for (unsigned int i = 0; i < pNames.size(); i++)
    {
	try
	{
	    IPCPartition p(pNames[i].c_str());
	    RunParamsNamed runParam(p, "RunParams.RunParams");
	    runParam.checkout();
	    std::string pName = pNames[i] + ".html";
	    m_outStream->open(pName);
	    Text pageName("Run Parameters:");
	    m_outStream->write(pageName);
	    m_outStream->write(br);
	    Table tabISInfo(2, 12);
	    tabISInfo.setBorder(1);
	    tabISInfo.addElement(1, 1, new Text("Names:"));
	    tabISInfo.addElement(2, 1, new Text("Values:"));

	    tabISInfo.addElement(1, 2, new Text("run_number"));
	    tabISInfo.addElement(2, 2, new Text(getLongString(runParam.run_number)));
	    tabISInfo.addElement(1, 3, new Text("max_events"));
	    tabISInfo.addElement(2, 3, new Text(getLongString(runParam.max_events)));

	    tabISInfo.addElement(1, 4, new Text("trigger_type"));
	    tabISInfo.addElement(2, 4, new Text(getLongString(runParam.trigger_type)));

	    tabISInfo.addElement(1, 5, new Text("run_type"));
	    tabISInfo.addElement(2, 5, new Text(runParam.run_type));

	    tabISInfo.addElement(1, 6, new Text("detector_mask"));
	    tabISInfo.addElement(2, 6, new Text(runParam.det_mask));

	    tabISInfo.addElement(1, 7, new Text("beam_type"));
	    tabISInfo.addElement(2, 7, new Text(getLongString(runParam.beam_type)));

	    tabISInfo.addElement(1, 8, new Text("beam_energy"));
	    tabISInfo.addElement(2, 8, new Text(getLongString(runParam.beam_energy)));

	    tabISInfo.addElement(1, 9, new Text("filename_tag"));
	    tabISInfo.addElement(2, 9, new Text(runParam.filename_tag));

	    std::string st = std::string(runParam.timeSOR.str());
	    tabISInfo.addElement(1, 10, new Text("timeSOR"));
	    tabISInfo.addElement(2, 10, new Text(st));

	    st = std::string(runParam.timeEOR.str());
	    tabISInfo.addElement(1, 11, new Text("timeEOR"));
	    tabISInfo.addElement(2, 11, new Text(st));

	    tabISInfo.addElement(1, 12, new Text("totalTime"));
	    tabISInfo.addElement(2, 12, new Text(getLongString(runParam.totalTime)));

	    m_outStream->write(tabISInfo);
	    m_outStream->close();
	}
	catch (...)
	{
	    std::string pName = pNames[i] + ".html";
	    std::string message = "Partition " + pNames[i]
		+ " does not have RunParams.RunParams information in IS server.";
	    m_outStream->createInfoPage(pName, message);
	}
    }
    m_outStream->closeAll();
}

void RunParamsPlugin::stop()
{
}

void RunParamsPlugin::configure(const ::wmi::InfoParameter& params)
{
}

