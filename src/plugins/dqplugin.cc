#include <wmi/plugins/dqplugin.h>

#include <list>
#include <memory>

#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>

#include <TH2.h>
#include <TH3.h>
#include <TError.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TEnv.h>
#include <TString.h>
#include <TClass.h>

#include <is/infodictionary.h>

#include <oh/OHRootReceiver.h>
#include <oh/OHUtil.h>
#include <rc/RunParamsNamed.h>

#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/AlgorithmConfigFactory.h>
#include <dqm_config/dal/DQAgent.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQReference.h>

#include <dqmf/is/Result.h>

#include <config/Configuration.h>
#include <config/Change.h>
#include <config/ConfigObject.h>

#include <dal/Segment.h>
#include <dal/Partition.h>
#include <dal/util.h>

#include <wmi/link.h>
#include <wmi/br.h>
#include <wmi/hr.h>
#include <wmi/utils.h>
#include <wmi/table.h>
#include <wmi/list.h>
#include <wmi/text.h>
#include <wmi/picture.h>
#include <wmi/tooltip.h>

using namespace daq::wmi;

namespace 
{
    const Color ORANGE (255,160,0);
    const Color YELLOW (255,220,0);
    const Color RED (255,0,0);
    const Color GREEN (0,255,0);
    const Color GREY (128,128,128);
    const Color WHITE (255,255,255);
    const Color BLACK (0,0,0);
    const Color DA_GREY (96,96,96);
    const Color LT_GREY (204,204,204);
    const Color DDA_MARINE (0,64,128);
    const Color DA_MARINE (55,113,172);
    const Color LT_MARINE (186,200,236);
    const Color LT_SAND (244,244,235);
    const Color DA_SAND (225,217,198);
}

#define TIMESTAMP_TEXT_OPTIONS		1
#define NORMAL_TEXT_OPTIONS		2
#define TITLE_TEXT_OPTIONS		3
#define PARAM_TEXT_OPTIONS		2, BLACK
#define PARAM_VAL_TEXT_OPTIONS		2, DDA_MARINE 
#define TAGS_VAL_TEXT_OPTIONS		2, DA_GREY
#define THR_VAL_TEXT_OPTIONS( c )	2, c

namespace
{
    const std::string dqm_is_server_name = "DQM";

    BaseHTMLObject *
    makeResultElement(	const std::string & full_name, 
    			const std::string & name, 
                        const IPCPartition & partition, 
                        const Color & caption_color,
                        const Color & text_color,
                        const std::string & webis_prefix )
    {
        Text * s = new Text( "<script type='text/javascript'>transformXML('"
                              + webis_prefix
                              + partition.name() + "/is/"
                              + dqm_is_server_name + "/"
                              + dqm_is_server_name + "." + name
                              + "', 'dqresult_full.xsl');</script>" );
        if (full_name == name )
        {
            return s;
        }
        
        Table * r = new Table( 1, 2 );
        r -> setWidth( 100, true );
        r -> setAlignCell( 1, 1, 2 ); // center
        r -> addElement( 1, 1, new Text( full_name, 2, BLACK, TextOption("b") ) );
        r -> addElement( 1, 2, s );
        return r;
    }

    std::string makePictureLink(const dqm_config::dal::DQParameter & param,
            const std::string & partition_name,
            const std::string & is_name,
            const std::string & webis_prefix,
            const std::string & picture_format,
            int w = -1, int h = -1) {
        std::string link = webis_prefix + partition_name + "/oh/" + is_name + "?type=" + picture_format;
        if ( w != -1 and h != -1 ) {
            link+= "&size=" + boost::lexical_cast<std::string>(w) + "x" + boost::lexical_cast<std::string>(h);
        }
        if (not param.get_DrawOption().empty()) {
            link += "&draw=" + param.get_DrawOption();
        }
        return link;
    };

    Table *
    makeParameterTable( const dqm_config::dal::DQParameter * dqparam, 
    			const IPCPartition & partition, 
                        const std::string & ref_name,
                        const std::string & ref_is_name,
                        const std::string & webis_prefix,
                        const std::string & picture_format )
    {
        std::string name = dqparam->UID();

	Table * table = new Table( 1, 2 );
        table -> setWidth( 100, true );
	table -> setCellPadding( 0 );
	table -> setCellSpacing( 0 );

	BaseHTMLObject * r = makeResultElement( name, name, partition,
        		LT_MARINE, BLACK, webis_prefix );
	table -> addElement( 1, 1, r );
	std::string descr = dqparam->get_Description();
        std::string troub = dqparam->get_Troubleshooting();
        std::map<std::string, double> paramAlg;
        dqparam->get_parameters(paramAlg);
        std::map<std::string, double> greenTh;
        dqparam->get_green_thresholds(greenTh);
        std::map<std::string, double> redTh;
        dqparam->get_red_thresholds(redTh);

	int rows = 2 + !ref_name.empty() + !descr.empty() + !troub.empty() + !paramAlg.empty()
	        + (!greenTh.empty() || !redTh.empty());

	Table * t = new Table( 2, rows );
	t -> setCellPadding( 2 );
	t -> setCellSpacing( 0 );
        t -> setWidth( 100, true );
        t -> setTDWidth( 2, 100, true );
        t -> addElement( 1, 1, new Text( "Histogram(s)&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
        t -> setVAlignCell( 1, 1, 1 );

        int row = 1;
        int ref_row = -1;
	const std::vector<std::string> & names = dqparam->get_InputDataSource();
	for ( unsigned int i = 0; i < names.size(); i++ ) {
	    std::string ref = makePictureLink(*dqparam, partition.name(), names[i], webis_prefix, picture_format);
	    t -> addElement( 2, row, new Link( ref, names[i] ) );
	    if ( i != ( names.size() - 1 ))
		t -> addElement( 2, row, new Text( "; " ) );
            t -> addElement( 2, row, new BR() );
	}

        ++row;
        t -> setVAlignCell( 1, row, 1 );
        t -> addElement( 1, row, new Text( "Algorithm&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
        t -> addElement( 2, row, new Text( dqparam->get_DQAlgorithm()->UID(), PARAM_VAL_TEXT_OPTIONS ) );

        if (!descr.empty()) {
            ++row;
            t -> addElement( 1, row, new Text( "Description&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
            t -> setVAlignCell( 1, row, 1 );
            t -> addElement( 2, row, new Tooltip( descr ) );
        }

        if (!troub.empty()) {
            ++row;
            t -> addElement( 1, row, new Text( "Troubleshooting&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
            t -> setVAlignCell( 1, row, 1 );
            t -> addElement( 2, row, new Tooltip( troub ) );
        }

	if (!ref_name.empty()) {
	    ref_row = ++row;
            t -> addElement( 1, row, new Text( "Reference&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
            t -> setVAlignCell( 1, row, 1 );
            std::string reflink = makePictureLink(*dqparam, partition.name(), ref_is_name, webis_prefix, picture_format);
            t -> addElement( 2, row, new Link( reflink, ref_name ) );
            t -> setBGColorCell( 1, row, DA_SAND );
            t -> setBGColorCell( 2, row, DA_SAND );
	}

	if (!paramAlg.empty()) {
            ++row;
            t -> addElement( 1, row, new Text( "Parameter(s)&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
            t -> setVAlignCell( 1, row, 1 );
            std::ostringstream out;
            std::map<std::string, double>::iterator it = paramAlg.begin();
            for( ; it != paramAlg.end(); ++it ) {
                out << it->first << " = " << it->second << "; ";
            }
            t -> addElement( 2, row, new Text( out.str(), PARAM_VAL_TEXT_OPTIONS ) );
	}

	if (!greenTh.empty() || !redTh.empty()) {
            ++row;
            t -> addElement( 1, row, new Text( "Threshold(s)&nbsp;&nbsp;", PARAM_TEXT_OPTIONS ) );
            t -> setVAlignCell( 1, row, 1 );
            std::map<std::string, double>::iterator git = greenTh.begin();
            for( ; git != greenTh.end(); ++git ) {
                std::string name = git->first;
                double gval = git->second;
                t -> addElement( 2, row, new Text( Utils::toString( gval ), THR_VAL_TEXT_OPTIONS( GREEN ) ) );

                std::map<std::string, double>::iterator rit = redTh.find(name);
                if ( rit != redTh.end() ) {
                    double rval = rit->second;
                    std::string sign = gval > rval ? ">" : "<";
                    t -> addElement( 2, row, new Text( sign, THR_VAL_TEXT_OPTIONS( BLACK ) ) );
                    t -> addElement( 2, row, new Text( name, THR_VAL_TEXT_OPTIONS( ORANGE ) ) );
                    t -> addElement( 2, row, new Text( sign, THR_VAL_TEXT_OPTIONS( BLACK ) ) );
                    t -> addElement( 2, row, new Text( Utils::toString( rval ), THR_VAL_TEXT_OPTIONS( RED ) ) );
                }
                t -> addElement( 2, row, new Text( "; ", THR_VAL_TEXT_OPTIONS( BLACK ) ) );
            }
        }

        for (int i = 1; i <= rows; i += 2) {
            if (i != ref_row) {
                t -> setBGColorCell( 1, i, LT_SAND );
                t -> setBGColorCell( 2, i, LT_SAND );
            }
        }

        table -> addElement( 1, 2, t );
	return table;
    }   
        
    Table *
    makeParametersTable( 
    		const dqm_config::dal::DQRegion * region, 
    		const IPCPartition & partition, 
                TCanvas * canvas,
                IPCPipeline & threadpool,
                dqm_config::AlgorithmConfigFactory* aconfig,
                unsigned int w,
                unsigned int h,
                unsigned int mask,
                const std::string & base_dir,
                const std::string & picture_format,
                bool show_histograms, 
                bool plot_references,
                const std::string & webis_prefix )
    {
	std::vector<const dqm_config::dal::DQParameter*> params;
	region->get_all_parameters(params);
	if ( !params.size() )
        {
            return 0;
        }
        
	Table * r = new Table( 4, params.size() );
	r -> setCellPadding( 2 );
	r -> setCellSpacing( 5 );
	r -> setWidth( 100, true );
	r -> setTDWidth( 1, 10 );
        
	for ( unsigned long i = 0; i < params.size(); ++i )
	{
	    int col = 2;
	    r -> setAlignCell( col, i + 1, 2 );
	    r -> setVAlignCell( col, i + 1, 3 );
	    r -> setWidthCell( col, i + 1, w + 2 );
            
            const std::vector<std::string> & names = params[i]->get_InputDataSource();
            auto hlink = makePictureLink(
                    *params[i], partition.name(), names[0], webis_prefix, picture_format, w, h );
            r -> addElement( col, i + 1, new Picture( hlink, w, h ) );

            ++col;
            const std::vector<const dqm_config::dal::DQReference*> & references = params[i]->get_DQReferences();
            std::string refname;
            std::string ref_is_name;
            if (not references.empty()) {
                try {
                    ref_is_name = aconfig->getReferenceManager().getReferenceName(params[i]->UID());
                    auto piclink = makePictureLink(
                            *params[i], partition.name(), ref_is_name, webis_prefix, picture_format, w, h );
                    r -> setAlignCell( col, i + 1, 2 );
                    r -> setVAlignCell( col, i + 1, 3 );
                    r -> setWidthCell( col, i + 1, w + 8 );

                    Table * bgcolor = new Table( 1, 1 );
                    bgcolor -> setBorder( 0 );
                    bgcolor -> setCellPadding( 4 );
                    bgcolor -> setCellSpacing( 0 );
                    bgcolor -> setBGColorCell( 1, 1, DA_SAND );
                    bgcolor -> setAlignCell( 1, 1, 2 );
                    bgcolor -> setVAlignCell( 1, 1, 3 );
                    bgcolor -> setWidthCell( 1, 1, 100, true );
                    bgcolor -> addElement( 1, 1, new Picture( piclink, w, h ) );

                    r -> addElement( col, i + 1, bgcolor );
                } catch ( ... ) { ; }
            }

            ++col;
            Table * p = makeParameterTable(
                    params[i], partition, refname, ref_is_name, webis_prefix, picture_format );
            r -> addElement( col, i + 1, p );
            r -> setVAlignCell( col, i + 1, 1 );
	}

	return r;
    }

    void 
    writeTitle( const std::string & title, OutputStream & out, int index )
    {
	Table t( 1, 1 );
	t.setWidth( 100, true );
	t.setBGColor( DA_MARINE );
	t.setAlignCell( 1, 1, 2 );
	t.addElement( 1, 1, new Text( title, TITLE_TEXT_OPTIONS, WHITE ) );
	out.write( t, index );
    }

    template <class T>
    void getAgents( Configuration& config,
		    const T& parent,
		    std::vector<const dqm_config::dal::DQAgent *>& agents )
    {
	daq::core::SegmentIterator si = parent.get_Segments().begin();
	for ( ; si != parent.get_Segments().end(); ++si )
	{
	    daq::core::BaseApplicationIterator ai = (*si)->get_Applications().begin();
	    daq::core::BaseApplicationIterator end = (*si)->get_Applications().end();
	    for ( ; ai != end; ++ai )
	    {
		const dqm_config::dal::DQAgent * a =
		    config.cast<dqm_config::dal::DQAgent,daq::core::BaseApplication>(*ai);
		if ( a )
		{
		    agents.push_back(a);
		}
	    }
	    getAgents(config, *(*si), agents);
	}
    }

    bool visit_regions( const dqm_config::dal::DQRegion * region, DQPlugin::VisitorParameter param )
    {
	if ( param.plugin->stopped() )
            return false;

        param.region = region;
        std::string region_name = param.region->UID();
        std::string region_file_name(region_name);
        std::replace( region_file_name.begin(), region_file_name.end(), '/', '_');

	ERS_DEBUG( 1, "Generating file for the '" << region_name << "' region" );

	param.parent_name = param.parent_name + "/"+ region_name;

	dqmf::is::Result result;
	BaseHTMLObject * r = makeResultElement( param.parent_name, region_name, 
        		param.partition, DA_MARINE, WHITE, param.webis_prefix() );
	param.fd = param.plugin->out()->open( region_file_name + ".html", param.partition.name() );
	param.plugin->out()->write( *r, param.fd );
	BR br;
	param.plugin->out()->write( br, param.fd );
	delete r;

	unsigned int row = (*param.parent_row)++;
	std::string page_name = param.base_name.empty()
				    ? region_file_name + ".html"
				    : param.base_name + "/" + region_file_name + ".html";
	param.parent_table -> addElement( 2, row, new Link( page_name, region_name ) );

        Text * t = new Text( "<script type='text/javascript'>transformXML('"
                              + param.webis_prefix()
                              + param.partition.name() + "/is/"
                              + dqm_is_server_name + "/"
                              + dqm_is_server_name + "." + region_name
                              + "', 'dqresult_short.xsl');</script>" );
        param.parent_table -> addElement( 6, row, t );
        
	param.parent_table -> setWidthCell( 1, 1, 20, false );
	param.parent_table -> setWidthCell( 3, 1, 20, false );
	param.parent_table -> setHeightCell( 3, 1, 20, false );

	std::vector<const dqm_config::dal::DQRegion*> regions;
	param.region->get_all_regions(regions);

	if (regions.size()) {
	    param.parent_table.reset(new Table(6, regions.size()));
	    param.parent_row.reset(new unsigned int(1));
	}
	else {
	    param.parent_table = std::shared_ptr<Table>();
	    param.parent_row = std::shared_ptr<unsigned int>();
	}
	param.base_name = "";

	for (size_t i = 0; i < regions.size(); i++) {
            if (!visit_regions(regions[i], param))
            	return false;
        }
        return true;
    }

}

DQPlugin::VisitorParameter::VisitorParameter(
	DQPlugin * pl,
	const IPCPartition & p,
	std::shared_ptr<Table> & t,
	unsigned int f )
  : plugin( pl ),
    region( 0 ),
    partition( p ),
    parent_name( p.name() ),
    base_name( p.name() ),
    parent_table( t ),
    parent_row( new unsigned int(1) ),
    fd( f )
{ ; }

DQPlugin::VisitorParameter::~VisitorParameter()
{
    if ( !plugin -> out() )
	return;

    if ( parent_table )
    {
	parent_table -> setCellPadding( 1 );
	plugin->out()->write( *parent_table, fd );
    }
    if ( region )
    {
	try 
        {
	    Table * r = makeParametersTable( region, partition,
                                             plugin->m_canvas, plugin->m_threadpool, plugin->m_aconfig,
					     plugin->m_picture_width, plugin->m_picture_height,
					     plugin->m_results_mask, plugin->m_base_dir,
					     plugin->m_picture_format,
					     plugin->m_show_histograms, plugin->m_plot_references,
					     plugin->m_webis_prefix );
	    if ( r )
	    {
		BR br;
		plugin->out()->write( br, fd );
		plugin->out()->write( *r, fd );
		delete r;
	    }
	}
	catch ( daq::config::Generic & ex )
	{
	    ERS_LOG( ex );
	}
    }

    if (plugin -> out() -> getFileDescription(fd)) {
	plugin -> out() -> getFileDescription(fd) -> addScriptFile( "transformxml.js" );
        plugin -> out() -> getFileDescription(fd) -> addHrefFile( "stylesheet", "text/css", "tooltip.css" );
    }
    plugin -> out() -> close( fd );
}

DQPlugin::DQPlugin()
  : m_stopped( false ),
    m_picture_height( 200 ),
    m_picture_width( 100 ),
    m_results_mask( (unsigned int)-1 ),
    m_show_histograms( false ),
    m_plot_references( false ),
    m_use_webis( false ),
    m_picture_format( "svg" ),
    m_webis_prefix( "/info/current/" ),
    m_canvas( 0 ),
    m_env( 0 ),
    m_threadpool( 1 ),
    m_aconfig( 0 )
{
    ;
}

DQPlugin::~DQPlugin()
{
    delete m_canvas;
    delete m_env;
}

bool 
DQPlugin::regenerateNecessary( std::list<IPCPartition> & lst )
{
    bool regenerate = false;
    // cleanup the map
    for( ActivePartitionsMap::iterator it = m_active_partitions.begin(); 
         it != m_active_partitions.end(); )
    {
    	if ( std::find( lst.begin(), lst.end(), it->first ) == lst.end() )
        {
            ERS_LOG("'" << it->first.name() << "' partition has been stopped.");
            it = m_active_partitions.erase( it );
            regenerate = true;
        }
        else
        {
            ++it;
        }
    }
    
    for( std::list<IPCPartition>::iterator pp = lst.begin(); pp != lst.end(); ++pp )
    {            
    	if ( pp->name().find( "mirror" ) != std::string::npos )
	    continue;

	ActivePartitionsMap::iterator it = m_active_partitions.find( *pp );
        
        if ( it != m_active_partitions.end() )
        {
            RunParamsNamed rp( *pp, "RunParams.RunParams" );
            try {
            	rp.checkout();
                if ( it->second != rp.run_number )
                {
                    ERS_LOG("Run number for the '" << pp->name()
                            << "' partition has been changed from " << it->second
                            << " to " << rp.run_number);
                    it->second = rp.run_number;
                    regenerate = true;
                }
            }
            catch( daq::is::Exception & )
            {
                ;
            }
            ERS_DEBUG( 1, "Partition '" << pp->name()
                    << "' is already in the active map, regenerate = " << regenerate );
        }
        else
        {
            if ( !m_partition.empty() && m_partition != pp->name() )
            {
                continue;
            }

	    try
            {
		std::string db_connect_string = "rdbconfig:" + pp->name() + "::RDB";
		std::unique_ptr<Configuration> config( new Configuration(db_connect_string));
		const daq::core::Partition* part_config = daq::core::get_partition( *config, pp->name() );
                if ( !part_config ) {
                    continue;
                }
		std::set<std::string> types;
                types.insert("DQAgent");
                std::vector<const daq::core::BaseApplication*> agents =
                        part_config->get_all_applications( &types );

                size_t rn = 0;
                for ( size_t i = 0; i < agents.size(); ++i )
                {
		    const dqm_config::dal::DQAgent * a =
		            config->cast<dqm_config::dal::DQAgent>(agents[i]);
                    rn += a->get_DQRegions().size();
                }
		if ( !rn ) {
		    continue;
		}
            }
            catch ( daq::config::Exception & ex)
            {
		ers::debug(ex, 1);
		continue;
            }

            RunParamsNamed rp( *pp, "RunParams.RunParams" );
            try
            {
                rp.checkout();
                m_active_partitions[*pp] = rp.run_number;
            }
            catch( daq::is::Exception & )
            {
                m_active_partitions[*pp] = 0;
            }

            regenerate = true;
            
            ERS_LOG( "Partition '" << pp->name() << "' has been added to the active map" );
        }
    }
    
    return ( regenerate );
}

bool 
DQPlugin::writePartition( const IPCPartition & partition )
{
    ERS_LOG( "Generating files for the '" << partition.name() << "' partition" );
    
    std::vector<const dqm_config::dal::DQRegion *> regions;
    std::unique_ptr<Configuration> config;
    try 
    {
	std::string db_connect_string = "rdbconfig:" + partition.name() + "::RDB";
	config.reset( new Configuration(db_connect_string) );

	const daq::core::Partition* part_config = daq::core::get_partition( *config, partition.name() );

	config->register_converter( new daq::core::SubstituteVariables( *part_config ) );
	dqm_config::dal::DQRegion::get_root_regions( *config, *part_config, regions );
    }
    catch ( daq::config::Generic & ex )
    {
    	ERS_LOG( ex );
    	return false;
    }
    
    if ( regions.empty() ) {
    	return false;
    }
    
    int index = m_outStream->open(partition.name() + ".html");
    m_base_dir = m_outStream -> getOutputDir() + m_outStream -> getPluginName()
            + "_new/" + partition.name() + "/";
    
    writeTitle( partition.name(), *m_outStream, index );
    
    BR br;
    m_outStream->write( br, index );
       
    m_aconfig = new dqm_config::AlgorithmConfigFactory(partition.name());

    std::shared_ptr<Table> table( new Table( 6, regions.size() ) );
    VisitorParameter parameter( this, partition, table, index );
    
    bool result = true;
    try
    {
	for( size_t i = 0; i < regions.size(); i++ ) {
            visit_regions( regions[i], parameter );
	    if ( stopped() )
		break ;
	}
    }
    catch ( ers::Issue & ex )
    {
    	ERS_LOG( ex );
        result = false;
    }
    
    m_threadpool.waitForCompletion();
    delete m_aconfig;
    
    return result;
}

void 
DQPlugin::periodicAction()
{
    Text ws = Text( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp" );
    std::list<IPCPartition> lst;
    IPCPartition::getPartitions(lst);

    if ( m_use_webis && !regenerateNecessary( lst ) )
    	return ;
        
    int index = m_outStream->open();
    writeTitle( "Partitions", *m_outStream, index );
    BR br;
    m_outStream->write( br, index );

    for( ActivePartitionsMap::iterator it = m_active_partitions.begin(); 
    	 it != m_active_partitions.end(); ++it )
    {
        if ( stopped() )
            break ;
            
    	if ( !m_partition.empty() && m_partition != it->first.name() )
        {
            continue;
        }
        
	if ( writePartition( it->first ) )
	{
	    Link ln( it->first.name() + ".html", it->first.name() );
	    m_outStream->write( ws, index );
	    m_outStream->write( ln, index );
	    m_outStream->write( br, index );
	}
    }
    m_outStream->close(index);
    m_outStream->closeAll();
}

void 
DQPlugin::configure( const ::wmi::InfoParameter& params )
{
    for( unsigned int i = 0; i < params.length(); ++i ) {
	std::string name( params[i].name );
        if( name == "picture_width" ) {
	    sscanf( (const char *)params[i].value, "%u", &m_picture_width );
            continue ;
	} 
	if ( name == "picture_height" ) {
	    sscanf( (const char *)params[i].value, "%u", &m_picture_height );
            continue ;
	}
	if ( name == "show_histograms" ) {
            if ( !::strcasecmp( (const char *)params[i].value, "yes" ) )
            	m_show_histograms = true;
            continue ;
	}
	if ( name == "use_webis" ) {
            if ( !::strcasecmp( (const char *)params[i].value, "yes" ) )
            	m_use_webis = true;
            continue ;
	}
	if ( name == "plot_references" ) {
            if ( !::strcasecmp( (const char *)params[i].value, "yes" ) )
            	m_plot_references = true;
            continue ;
	}
	if ( name == "picture_format" ) {
	    m_picture_format = (const char *)params[i].value;
            continue ;
	}
	if ( name == "webis_prefix" ) {
	    m_webis_prefix = (const char *)params[i].value;
            continue ;
	}
	if ( name == "partition_name" ) {
	    m_partition = (const char *)params[i].value;
            continue ;
	}
	if ( name == "results_mask" ) {
	    sscanf( (const char *)params[i].value, "%u", &m_results_mask );
            continue ;
	}
    }

    m_canvas = new TCanvas( "c1", "c1", m_picture_width, m_picture_height );
    unsigned int w = m_picture_width + (m_picture_width - m_canvas -> GetWw());
    unsigned int h = m_picture_height + (m_picture_height - m_canvas -> GetWh());
    delete m_canvas;
    m_canvas = new TCanvas( "c1", "c1", w, h );
    
    gErrorIgnoreLevel = kFatal;
    gEnv -> SetValue( "Root.ErrorIgnoreLevel", "Fatal" );
}

void 
DQPlugin::stop()
{
    ERS_DEBUG( 0, "Stopping plugin" );
    m_stopped = true;
}

