#include "wmi/plugins/testplugin.h"
#include "wmi/utils.h"
#include "wmi/table.h"
#include "wmi/text.h"

#include "wmi/iframe.h"
#include <utility>

using namespace daq::wmi;

TestPlugin::TestPlugin()
{
}

TestPlugin::~TestPlugin()
{
    delete m_outStream;
}

void TestPlugin::periodicAction()
{

    Table table(4, 7);
    for (int i = 1; i <= 4; i++)
	for (int j = 1; j <= 7; j++)
	    table.addElement(i, j, new Text("test"));
    table.setTDWidth(2, 40);
    table.setTDWidth(3, 60);
    m_outStream->open();
    m_outStream->write(table);
    m_outStream->close();
}

void TestPlugin::stop()
{
}

void TestPlugin::configure(const ::wmi::InfoParameter& params)
{
}

