#include "wmi/frame.h"

using namespace daq::wmi;

Frame::Frame(const std::string &src, const std::string &name)
{
    m_src = src;
    m_name = name;
    m_margin_width = -1;
    m_margin_height = -1;
    m_scroll = NODEFINE;
    m_noresize = false;
    m_border = false;
    m_border_color = Color(false);
}

Frame::~Frame()
{
}

void Frame::setMarginWidth(unsigned int width)
{
    m_margin_width = (int) width;
}

void Frame::setMarginHeight(unsigned int height)
{
    m_margin_height = (int) height;
}

void Frame::setScroll(Scrolling scroll)
{
    m_scroll = scroll;
}

void Frame::setNoResize()
{
    m_noresize = true;
}

void Frame::setFrameBorder(bool border)
{
    m_border = border;
}

std::ostream& Frame::write(std::ostream& str) const
{
    str << "<frame src=\"" << m_src << "\" ";
    if (m_name.size() > 0)
	str << "name=\"" << m_name << "\" ";
    if (m_margin_width > 0)
	str << "marginwidth=\"" << m_margin_width << "\" ";
    if (m_margin_height > 0)
	str << "marginheight=\"" << m_margin_height << "\" ";
    if (m_scroll != NODEFINE)
    {
	str << "scrolling=\"";
	if (m_scroll == YES)
	    str << "yes";
	else if (m_scroll == NO)
	    str << "no";
	else
	str << "auto";
	str << "\" ";
    }
    if (m_noresize)
	str << "noresize ";
    if (m_border_color.getColor().size() > 0)
	str << "bordercolor=\"#" << m_border_color.getColor() << "\" ";
    if (m_border)
	str << "frameborder=\"yes\"";
    str << ">";

    return str;
}

std::ostream& daq::wmi::operator<<(std::ostream& str, const FrameBase& param)
{
    return param.write(str);
}
