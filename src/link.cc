#include "wmi/link.h"

using namespace daq::wmi;

Link::Link(const std::string & href, const std::string &text, bool need_replace)
{
    if (need_replace)
	m_href = BaseHTMLObject::replaceSpecialSymbol(href);
    else
    m_href = href;
    m_text = text;
    m_bgcolor = Color(false);
}

Link::~Link()
{
}

std::ostream& Link::write(std::ostream& str) const
{
    str << "<a href=\"" << m_href << "\">" << m_text << "</a>\n";
    return str;
}

