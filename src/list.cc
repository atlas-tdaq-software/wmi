#include "wmi/list.h"

using namespace daq::wmi;

List::List(int order)
{
    m_font = Font();
    m_bgcolor = Color(false);
    m_order = order;
}

List::List()
{
    m_font = Font();
    m_bgcolor = Color(false);
    m_order = 0;
}

List::~List()
{
}

void List::addElementAt(const std::string & elem, unsigned int i)
{
    if ((i <= m_list.size() && i > 0) || i == 0)
    {
	m_list.insert(m_list.begin() + i, elem);
    }
}

void List::addFirstElement(const std::string & elem)
{
    m_list.insert(m_list.begin(), elem);
}

void List::addLastElement(const std::string & elem)
{
    m_list.insert(m_list.end(), elem);
}

void List::deleteElementAt(unsigned int i)
{
    if ((i < m_list.size() && i > 0) || i == 0)
    {
	m_list.erase(m_list.begin() + i);
    }
}

void List::deleteFirstElement()
{
    m_list.erase(m_list.begin());
}

void List::deleteLastElement()
{
    m_list.erase(m_list.end());
}

std::ostream& List::write(std::ostream& str) const
{
    if (m_order > 0)
    {
	std::string st;
	switch (m_order)
	{
	    case 1:
		st = "A";
		break;
	    case 2:
		st = "a";
		break;
	    case 3:
		st = "I";
		break;
	    case 4:
		st = "i";
		break;
	    case 5:
		st = "1";
		break;
	    default:
		st = "1";
		break;

	}
	str << "<ol type=\"" << st << "\">\n";
    }
    else
    {
	str << "<ul>\n";
    }
    for (unsigned int i = 0; i < m_list.size(); i++)
    {
	str << "<li>" << m_list[i] << "</li>\n";
    }
    if (m_order > 0)
	str << "</ol>\n";
    else
    str << "</ul>\n";
    return str;
}

