#include "wmi/filedescription.h"
#include <sstream>

using namespace daq::wmi;

std::string toString(unsigned int i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}
FileDescription::FileDescription(const std::string &http_root)
{
    m_max_id = 0;
    m_variable_number = 0;
    m_http_root = http_root;
}
void FileDescription::addScriptFile(const std::string & path)
{
    for (unsigned int i = 0; i < m_script_file.size(); i++)
    {
	if (m_script_file[i].compare(path) == 0)
	    return;
    }
    m_script_file.push_back(path);
}

void FileDescription::addHrefFile(const std::string &_rel, const std::string &_type,
    const std::string &_href)
{
    for (unsigned int i = 0; i < m_href_file.size(); i++)
    {
	HrefFile l = m_href_file[i];
	if (l.rel.compare(_rel) == 0 && l.type.compare(_type) == 0
	    && l.href.compare(_href) == 0)
	    return;
    }
    HrefFile l;
    l.rel = _rel;
    l.type = _type;
    l.href = _href;
    m_href_file.push_back(l);
}

std::string FileDescription::getNewId()
{
    std::string id = "id";
    id += toString(m_max_id);
    m_max_id++;
    return id;
}

std::string FileDescription::getNewVariable()
{
    std::string id = "variable";
    id += toString(m_variable_number);
    m_variables.push_back(id);
    m_variable_number++;
    return id;
}

std::vector<std::string> FileDescription::getScriptFiles()
{
    return m_script_file;
}

std::vector<HrefFile> FileDescription::getHrefFiles()
{
    return m_href_file;
}

std::string FileDescription::getHTTPRoot()
{
    return m_http_root;
}
