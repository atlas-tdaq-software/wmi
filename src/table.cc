#include <stdio.h>
#include "wmi/table.h"

using namespace daq::wmi;

CellOption::CellOption()
{
    m_bgcolor = Color(false);
}

Table::Table()
{
    m_cellspacing = -1;
    m_cellpadding = -1;
    m_columns = 1;
    m_lines = 1;
    m_border = 0;
    m_htmlObjs.resize(1);
    m_tdWidth.resize(1, "");
    m_bgcolor = Color(false);
    m_font = Font();
}

Table::Table(unsigned int column, unsigned int line)
{
    m_cellspacing = -1;
    m_cellpadding = -1;
    m_htmlObjs.resize(column * line);
    m_tdWidth.resize(column, "");
    m_columns = column;
    m_lines = line;
    m_border = 0;
    m_bgcolor = Color(false);
    m_font = Font();
}

Table::~Table()
{
    for (unsigned int i = 0; i < m_lines * m_columns; i++)
    {
	for (unsigned int j = 0; j < m_htmlObjs[i].first.size(); j++)
	    delete ((m_htmlObjs[i]).first[j]);
    }
}

void Table::addRow()
{
    m_lines++;
    m_htmlObjs.resize(m_columns * m_lines);
}

void Table::setBorder(int br)
{
    m_border = br;
}

void Table::setCellSpacing(int br)
{
    m_cellspacing = br;
}

void Table::setCellPadding(int br)
{
    m_cellpadding = br;
}

void Table::setWidth(unsigned int w, bool isPercent)
{
    if (isPercent && w > 100)
	w = 100;
    char ch[20];
    sprintf(ch, "%d", w);
    m_width = std::string(ch);
    if (isPercent)
	m_width += "%";
}

void Table::setHeight(unsigned int h, bool isPercent)
{
    if (isPercent && h > 100)
	h = 100;
    char ch[20];
    sprintf(ch, "%d", h);
    m_height = std::string(ch);
    if (isPercent)
	m_height += "%";
}

void Table::setTDWidth(unsigned int number, unsigned int w, bool isPercent)
{
    if (number > 0)
	number--;
    if (number > m_columns)
	return;
    //number = m_columns;
    if (isPercent && w > 100)
	w = 100;
    char ch[20];
    sprintf(ch, "%d", w);
    for (unsigned int i = 0; i < m_lines; i++)
    {
	(m_htmlObjs[number + i * m_columns]).second.m_width = std::string(ch);
	if (isPercent)
	    (m_htmlObjs[number + i * m_columns]).second.m_width += "%";
    }
}

void Table::setWidthCell(unsigned int i, unsigned int j, unsigned int w, bool isPercent)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i > m_columns) || (j > m_lines))
	return;
    char ch[20];
    sprintf(ch, "%d", w);
    (m_htmlObjs[i + j * m_columns]).second.m_width = std::string(ch);
    if (isPercent)
	(m_htmlObjs[i + j * m_columns]).second.m_width += "%";
}

void Table::setHeightCell(unsigned int i, unsigned int j, unsigned int w, bool isPercent)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i > m_columns) || (j > m_lines))
	return;
    char ch[20];
    sprintf(ch, "%d", w);
    (m_htmlObjs[i + j * m_columns]).second.m_height = std::string(ch);
    if (isPercent)
	(m_htmlObjs[i + j * m_columns]).second.m_height += "%";
}

void Table::setAlignCell(unsigned int i, unsigned int j, unsigned int number)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i > m_columns) || (j > m_lines))
	return;
    if (number == 1)
	(m_htmlObjs[i + j * m_columns]).second.m_align = "left";
    else if (number == 2)
	(m_htmlObjs[i + j * m_columns]).second.m_align = "center";
    else if (number == 3)
	(m_htmlObjs[i + j * m_columns]).second.m_align = "right";
}

void Table::setVAlignCell(unsigned int i, unsigned int j, unsigned int number)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i > m_columns) || (j > m_lines))
	return;
    if (number == 1)
	(m_htmlObjs[i + j * m_columns]).second.m_valign = "top";
    else if (number == 2)
	(m_htmlObjs[i + j * m_columns]).second.m_valign = "bottom";
    else if (number == 3)
	(m_htmlObjs[i + j * m_columns]).second.m_valign = "middle";
}

void Table::setBGColorCell(unsigned int i, unsigned int j, Color col)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i > m_columns) || (j > m_lines))
	return;
    m_htmlObjs[i + j * m_columns].second.m_bgcolor = col;
}

bool Table::addElement(unsigned int i, unsigned int j, BaseHTMLObject* Obj)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i <= m_columns) && (j <= m_lines))
    {
	(m_htmlObjs[i + j * m_columns]).first.push_back(Obj);
	return true;
    }
    return false;
}

std::ostream& Table::write(std::ostream& str) const
{
    str << "<table";
    if (m_bgcolor.getColor().size() > 0)
	str << " bgcolor=\"#" << m_bgcolor.getColor() << "\"";
    if (m_width.size() > 0)
	str << " width=\"" << m_width << "\"";
    if (m_height.size() > 0)
	str << " height=\"" << m_height << "\"";
    if (m_border > 0)
	str << " border=\"" << m_border << "\"";
    if (m_cellspacing > -1)
	str << " cellspacing=\"" << m_cellspacing << "\"";
    if (m_cellpadding > -1)
	str << " cellpadding=\"" << m_cellpadding << "\"";
    str << ">\n";
    for (unsigned int j = 0; j < m_lines; j++)
    {
	str << "<tr>";
	for (unsigned int i = 0; i < m_columns; i++)
	{
	    bool isElemetn = false;
	    str << "<td";
	    if (m_htmlObjs[i + j * m_columns].second.m_bgcolor.getColor().size() > 0)
		str << " bgcolor=\"#"
		    << m_htmlObjs[i + j * m_columns].second.m_bgcolor.getColor() << "\"";

	    if (m_htmlObjs[i + j * m_columns].second.m_align.size() > 0)
		str << " align=\"" << m_htmlObjs[i + j * m_columns].second.m_align
		    << "\"";
	    if (m_htmlObjs[i + j * m_columns].second.m_valign.size() > 0)
		str << " valign=\"" << m_htmlObjs[i + j * m_columns].second.m_valign
		    << "\"";
	    if (m_htmlObjs[i + j * m_columns].second.m_height.size() > 0)
		str << " height=\"" << m_htmlObjs[i + j * m_columns].second.m_height
		    << "\"";

	    if (m_htmlObjs[i + j * m_columns].second.m_width.size() > 0)
		str << " width=\"" << m_htmlObjs[i + j * m_columns].second.m_width
		    << "\"";
	    str << ">";
	    for (unsigned int ii = 0; ii < (m_htmlObjs[i + j * m_columns]).first.size();
		ii++)
	    {
		(m_htmlObjs[i + j * m_columns]).first[ii]->setPluginName(m_pluginName);
		(m_htmlObjs[i + j * m_columns]).first[ii]->setOutputDir(m_outputDir);
		(m_htmlObjs[i + j * m_columns]).first[ii]->m_fd = m_fd;
		if ((m_htmlObjs[i + j * m_columns]).first[ii]->getBGColor().getColor().size()
		    == 0)
		{
		    if (m_htmlObjs[i + j * m_columns].second.m_bgcolor.getColor().size()
			> 0)
			(m_htmlObjs[i + j * m_columns]).first[ii]->setBGColor(
			    m_htmlObjs[i + j * m_columns].second.m_bgcolor);
		    else if (m_bgcolor.getColor().size() > 0)
			(m_htmlObjs[i + j * m_columns]).first[ii]->setBGColor(m_bgcolor);
		}
		isElemetn = true;
		str << (*(m_htmlObjs[i + j * m_columns]).first[ii]);

	    }
	    if (!isElemetn)
		str << "&nbsp";
	    str << "</td>\n";
	}
	str << "</tr>\n";
    }
    str << "</table>";
    return str;
}

