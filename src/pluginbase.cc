#include"wmi/pluginbase.h"

using namespace daq::wmi;

std::string PluginBase::getPluginName()
{
    return m_outStream->getPluginName();
}

unsigned int PluginBase::getStatus()
{
    return m_status;
}

void PluginBase::setStatus(unsigned int status)
{
    m_status = status;
}

void PluginBase::configure(const ::wmi::InfoParameter& params, OutputStream* oStream)
{
    m_outStream = oStream;
    configure(params);
}
