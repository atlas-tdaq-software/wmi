#include "wmi/outputstream.h"
#include "wmi/text.h"
#include "wmi/br.h"
using namespace daq::wmi;

void OutputStream::setPluginName(std::string st)
{
    m_pluginName = st;
}

void OutputStream::setOutputDir(std::string st)
{
    m_outputDir = st;
}

void OutputStream::setScriptDir(std::string st)
{
    m_scriptDir = st;
}

void OutputStream::setPageName(std::string name)
{
    m_pageName = name;
}

std::string OutputStream::getOutputDir()
{
    return m_outputDir;
}

FileDescription* OutputStream::getFileDescription(int ind)
{
    std::map<int, void*>::iterator myIterator = m_file_description_map.find(ind);
    if (myIterator != m_file_description_map.end())
	return (FileDescription*) (*myIterator).second;
    return NULL;
}

std::string OutputStream::getPluginName()
{
    return m_pluginName;
}

void OutputStream::setUseScript(bool value)
{
    m_useScript = value;
}

void OutputStream::setCurrentIndex(int ind)
{
    if (ind > -1)
	m_currentIndex = ind;
}

void OutputStream::close()
{
    close(m_currentIndex);
}

void OutputStream::write(BaseHTMLObject& tb)
{
    write(tb, m_currentIndex);
}

void OutputStream::deleteIndexFromMap(int ind)
{
    if (m_index_map.empty())
	return;
    std::map<std::string, int>::iterator myIterator;
    for (myIterator = m_index_map.begin(); myIterator != m_index_map.end(); ++myIterator)
    {
	if ((*myIterator).second == ind)
	{
	    m_index_map.erase(myIterator);
	    return;
	}
    }
}

int OutputStream::getFileDescriptor(std::string fname)
{
    fname = BaseHTMLObject::replaceSpecialSymbol(fname);
    std::map<std::string, int>::iterator myIterator;
    myIterator = m_index_map.find(fname);
    if (myIterator != m_index_map.end())
	return (*myIterator).second;
    return -1;
}

std::string OutputStream::getFileName(int ind)
{
    std::map<std::string, int>::iterator myIterator = m_index_map.begin();
    while (myIterator != m_index_map.end())
    {
	if ((*myIterator).second == ind)
	    return (*myIterator).first;
	++myIterator;
    }
    return "";
}

int OutputStream::getNewIndex()
{
    std::map<int, void*>::iterator myIterator;
    for (int i = m_maxIndex; i < 32000; i++)
    {
	myIterator = m_file_map.find(i);
	if (myIterator == m_file_map.end())
	{
	    m_maxIndex = i;
	    return i;
	}
    }
    for (int i = 0; i < 32000; i++)
    {
	myIterator = m_file_map.find(i);
	if (myIterator == m_file_map.end())
	{
	    m_maxIndex = i;
	    return i;
	}
    }
    return -1;
}

void OutputStream::createInfoPage(std::string fileName, std::string message)
{
    fileName = BaseHTMLObject::replaceSpecialSymbol(fileName);
    open(fileName);
    Text t(message);
    BR br;
    write(t);
    write(br);
    write(br);
    write(br);
    close();
}
