#include "wmi/tree.h"

using namespace daq::wmi;

Node::Node(const std::string& name, bool isVisible)
{
    m_value = name;
    m_previous = NULL;
    m_visible = isVisible;
}

Node::~Node()
{
    for (unsigned int i = 0; i < m_children.size(); i++)
	delete m_children[i];
    m_children.clear();
}

unsigned int Node::getChildrenCount()
{
    return m_children.size();
}

void Node::appendChildren(Node* node)
{
    m_children.push_back(node);
    node->setParent(this);
}

void Node::setParent(Node* node)
{
    m_previous = node;
}

Node* Node::getParent()
{
    return m_previous;
}

Node* Node::getChild(unsigned int num)
{
    if (m_children.size() < num)
	return NULL;
    return m_children.at(num);
}

Node* Node::getChildConst(unsigned int num) const
{
    if (m_children.size() < num)
	return NULL;
    return m_children.at(num);
}

Node* Node::appendChildren(const std::string& name, bool isVisible)
{
    Node* node = new Node(name, isVisible);
    appendChildren(node);
    return node;
}

Tree::Tree(const std::string & name, Node* node, unsigned int width, unsigned int height)
{
    m_name = name;
    m_node = node;
    m_width = width;
    m_height = height;
}
//
Tree::~Tree()
{
    delete m_node;
}

void Tree::writeNode(std::ostream& str, const Node* node, std::string prev_id) const
{
    if (node->m_children.size() == 0)
    {
	str << node->m_value << "/]";
	return;
    }
    else
    str << node->m_value << "/<";
    for (unsigned int i = 0; i < node->m_children.size(); i++)
    {
	Node* cnode = node->getChildConst(i);
	if (cnode != NULL)
	{
	    writeNode(str, cnode, "");
	}
    }
    str << "/>";
}

std::ostream& Tree::write(std::ostream& str) const
{
    str << "<object classid=\"clsid:CAFEEFAC-0016-0000-0000-ABCDEFFEDCBA\" name="
	<< m_name << " id=" << m_name << " width=";
    str << m_width << " height=" << m_height
	<< " codebase=\"http://java.sun.com/javase/downloads/ea.jsp\">\n";
    str << "<param name=\"type\" value=\"application/x-java-applet;version=1.5\">\n";
    str << "<param name=\"code\" value=\"wmi.tree.WMITree.class\">\n";
    str << "<param name=\"codebase\" value=\"" << m_fd->getHTTPRoot() + "/applet"
	<< "\">\n";
    str << "<param name=\"archive\" value=\"tree.jar\">\n";
    str << "<param name=\"title\" value=\"" << m_name << "\">\n";
    if (m_bgcolor.getColor().size() > 0)
	str << "<param name=\"bgcolor\" value=\"" << m_bgcolor.getColor() << "\">\n";
    str << "<param name=\"tree\" value=\"";
    if (m_node != NULL)
    {
	if (m_node->m_children.size() == 0)
	    str << m_node->m_value << "/]";
	else
	{
	    str << m_node->m_value << "/<";
	    for (unsigned int i = 0; i < m_node->getChildrenCount(); i++)
	    {
		Node* cnode = m_node->getChildConst(i);
		if (cnode != NULL)
		{
		    writeNode(str, cnode, "");
		}
	    }
	    str << "/>";
	}
    }
    str << "\">\n";
    str << "<comment>\n";
    str
	<< "<embed type=\"application/x-java-applet;version=1.5\" code=wmi.tree.WMITree.class java_code=wmi.tree.WMITree.class ";
    str << "width=" << m_width << " height=" << m_height
	<< " pluginspage=\"http://java.sun.com/javase/downloads/ea.jsp\"";
    str << "codebase='" << m_fd->getHTTPRoot() + "/applet" << "' archive='tree.jar'";
    str << " title=\"" << m_name << "\"";
    if (m_bgcolor.getColor().size() > 0)
	str << " bgcolor=\"" << m_bgcolor.getColor() << "\"";
    str << " tree=\"";
    if (m_node != NULL)
    {
	if (m_node->m_children.size() == 0)
	    str << m_node->m_value << "/]";
	else
	{
	    str << m_node->m_value << "/<";
	    for (unsigned int i = 0; i < m_node->getChildrenCount(); i++)
	    {
		Node* cnode = m_node->getChildConst(i);
		if (cnode != NULL)
		{
		    writeNode(str, cnode, "");
		}
	    }
	    str << "/>";
	}
    }
    str << "\"/>\n</comment>\n";
    str << "</object>\n";

    return str;
}
