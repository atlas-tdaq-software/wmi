#include"wmi/server/wmiserver.h"
#include "wmi/link.h"
#include "wmi/br.h"
#include "wmi/text.h"

using namespace daq::wmi;
bool STOP = false;

WMIServer* current = 0;

WMIServer::WMIServer(const IPCPartition & p, const std::string & name)
    : WMIIPCNamedObject(p, name),
      m_local(false)
{
    publish();
}

WMIServer::~WMIServer()
{
}

void WMIServer::setPageName(OutputStream* out, std::string name)
{
    out->m_pageName = name;
}

void WMIServer::setPluginName(OutputStream* out, std::string name)
{
    out->m_pluginName = name;
}

void WMIServer::setOutputDir(OutputStream* out, std::string name)
{
    out->m_outputDir = name;
}

void WMIServer::setScriptDir(OutputStream* out, std::string name)
{
    out->m_scriptDir = name;
}

void WMIServer::setUseScript(OutputStream* out, bool value)
{
    out->m_useScript = value;
}

std::string WMIServer::getHTTPServerRoot()
{
    return m_httpServerRoot;
}

void WMIServer::copyScriptToServer(std::string path)
{
    if (m_outputImpl.compare("WMIHTMLOutput") == 0)
    {
	std::string http_path = m_httpRootDir + "scripts/";

	if (m_local)
	{
	    std::string com = "rsync -avz '" + path + "' '" + http_path + "'";
	    system(com.c_str());

	    com = "chmod -R u+w,g+w '" + http_path + "'";
	    system(com.c_str());
	}
	else
	{
	    std::string com = "rsync -avz '" + path + "' '" + m_httpServer + ":"
		+ http_path + "'";
	    system(com.c_str());

	    com = "ssh -nfx " + m_httpServer + " \"chmod -R u+w,g+w '" + http_path
		+ "'\"";
	    system(com.c_str());
	}
    }
}

void
WMIServer::copyToHTTPServer(const std::string & name)
{
    if (m_outputImpl.compare("WMIHTMLOutput"))
    {
	ers::error(ers::Message(ERS_HERE, "Cannot load output class implementation"));
	return;
    }

    std::string comDel = "rm -rf '" + m_outputDir + name + "_new'";
    std::string com;
    if (m_local)
    {
	std::string src = m_outputDir + name + "_new/";
	std::string dst = m_httpRootDir + name + "_new";
	std::string old = m_httpRootDir + name + "_old";
	std::string cur = m_httpRootDir + name + "_wmi";
	com = "[ \"$(ls -A '" + src + "' 2>/dev/null)\" ] && { cp -r '" + src + "' '" + dst + "'; ";
	com += "mv '" + cur + "' '" + old + "'; ";
	com += "mv '" + dst + "' '" + cur + "'; ";
	com += "rm -rf '" + old + "'; }";
    }
    else
    {
	std::string archive = name + "_new.tgz";
	com = "cd '" + m_outputDir + "'; tar zhcf '" + archive + "' '" + name + "_new'";
	int ret = system(com.c_str());
	if (ret == -1)
	{
	    ers::error(ers::Message(ERS_HERE, "Cannot create tar file"));
	    return;
	}

	com = "scp -r -q '" + m_outputDir + archive + "' '" + m_httpServer + ":"
	    + m_httpRootDir + "'";
	ret = system(com.c_str());
	if (ret == -1)
	{
	    ers::error(ers::Message(ERS_HERE, "Cannot copy data to HTTP Server"));
	    return;
	}

	std::string final_dir_name = name + "_wmi";

	com = "ssh -nfx " + m_httpServer +
	    "  \" cd '" + m_httpRootDir + "';"
	    + " tar xzf '" + archive + "';"
	    + " mv '" + final_dir_name + "' '" + name + "_old';"
	    + " mv '" + name + "_new' '" + final_dir_name + "';"
	    + " rm -rf '" + name + "_old';"
	    + " chmod -R g+w '" + final_dir_name + "';"
	    + " rm -f '" + archive + "'\"";

	comDel += "; rm -rf '" + m_outputDir + archive + "';";
    }

    ERS_DEBUG(1, "executing '" << com << "' command ");

    int ret = system(com.c_str());
    if (ret == -1)
    {
	ers::error(ers::Message(ERS_HERE, "Copying procedure failed"));
    }
    ret = system(comDel.c_str());
    if (ret == -1)
    {
	ers::error(ers::Message(ERS_HERE, "Cannot delete data from the local tmp area"));
    }
}

bool alarm_function(void * rock)
{
    ERS_DEBUG(1, "Starting working cycle ...");
    PluginBase* plugin = (PluginBase*) rock;

    try {
	plugin->periodicAction();
	if (!STOP) {
	    ERS_DEBUG(1, "Starting copying procedure ...");
	    current->copyToHTTPServer(plugin->getPluginName());
	    ERS_DEBUG(1, "Copying procedure completed");
	}
    }
    catch(ers::Issue & ex) {
	ers::error(ex);
    }
    catch(...) {
	std::ostringstream out;
	out << "Unknown exception has been thrown by the '" << plugin->getPluginName() << "' plugin";
	ers::error(ers::Message(ERS_HERE, out.str()));
    }

    ERS_DEBUG(1, "Working cycle completed");
    return !STOP;
}

void* start_plugin(void *p)
{
    ::wmi::StorePluginInfo* pluginInfo = (::wmi::StorePluginInfo*) p;
    OutputStream* outSt;
    std::string outputImpl = std::string(pluginInfo->outputImpl);
    if (outputImpl.compare("WMIHTMLOutput") == 0)
    {
	unsigned int ui = 30;
	if (pluginInfo->period > 30)
	    ui = (unsigned int) pluginInfo->period;
	outSt = new HTMLOutputStream(ui);
	std::string name = std::string(pluginInfo->name);
	current->setPageName(outSt, name);
    }
    else
    {
	ers::error(ers::Message(ERS_HERE, "Cannot load output class implementation"));
	return 0;
    }
    current->setPluginName(outSt, std::string(pluginInfo->name));
    current->setOutputDir(outSt, std::string(pluginInfo->outputDir));
    std::string http_server_root = current->getHTTPServerRoot();
    if (http_server_root[http_server_root.size() - 1] != '/')
	http_server_root += "/";
    http_server_root += "scripts/";
    current->setScriptDir(outSt, http_server_root);
    current->setUseScript(outSt, pluginInfo->useScript);

    void* lib = dlopen((std::string(pluginInfo->fullPath)).c_str(),
	RTLD_LAZY | RTLD_GLOBAL);
    if (!lib)
    {
	std::ostringstream txt;
	txt << "Cannot load library: " << dlerror();
	ers::error(ers::Message(ERS_HERE, txt.str()));
	return 0;
    }

    // reset errors
    dlerror();

    // load the symbols
    create_t* create_PluginBase = (create_t*) dlsym(lib, "create");
    const char* dlsym_error = dlerror();
    if (dlsym_error)
    {
	std::ostringstream txt;
	txt << "Cannot load symbol create: " << dlsym_error;
	ers::error(ers::Message(ERS_HERE, txt.str()));
	return 0;
    }

    destroy_t* destroy_PluginBase = (destroy_t*) dlsym(lib, "destroy");
    dlsym_error = dlerror();
    if (dlsym_error)
    {
	std::ostringstream txt;
	txt << "Cannot load symbol destroy: " << dlsym_error;
	ers::error(ers::Message(ERS_HERE, txt.str()));
	return 0;
    }

    // create an instance of the plugin
    PluginBase* pluginBase = create_PluginBase();

    ::wmi::InfoParameter params;
    params.length((pluginInfo->params).length());
    for (unsigned int i = 0; i < (pluginInfo->params).length(); i++)
	params[i] = pluginInfo->params[i];
    pluginBase->configure(params, outSt);

    if (pluginInfo->period > 0)
    {
	ERS_LOG("starting '" << pluginInfo->name << "' plugin");
	alarm_function(pluginBase);

	IPCAlarm alarm(pluginInfo->period, alarm_function, pluginBase); //pluginInfo->period

	while (!STOP)
	{
	    ::sleep(1);
	}

	ERS_LOG("stopping '" << pluginInfo->name << "' plugin");
	pluginBase->stop();
    }

    // destroy the plugin object
    destroy_PluginBase(pluginBase);

    return 0;
}

void WMIServer::shutdown()
{
    stopPlugins();
    daq::ipc::signal::raise();
}

void WMIServer::initialize(const ::wmi::InfoPlugin& plInfos,
    const ::wmi::ServerParameters& sParams)
{
    m_outputDir = std::string(sParams.outputDir);
    if (m_outputDir.size() && m_outputDir[m_outputDir.size() - 1] != '/')
	m_outputDir += "/";

    m_httpServer = std::string(sParams.httpServer);
    if (m_httpServer == "localhost")
	m_local = true;

    m_httpRootDir = std::string(sParams.httpRootDir);
    if (m_httpRootDir.size() && m_httpRootDir[m_httpRootDir.size() - 1] != '/')
	m_httpRootDir += "/";

    m_outputImpl = std::string(sParams.outputImpl);
    m_httpServerRoot = std::string(sParams.httpServerRoot);
    current = this;
    std::string install_script_dir = std::string(sParams.scriptDir);
    if (install_script_dir.length() > 0)
	copyScriptToServer(install_script_dir);

    for (unsigned int i = 0; i < plInfos.length(); i++)
    {
	m_infoPlugins.push_back(plInfos[i]);
    }

    std::string com("mkdir -p ");
    com += m_outputDir;
    int ret = system(com.c_str());
    if (ret == -1)
    {
        ers::error(ers::Message(ERS_HERE, "Can not create output directory"));
    }
}

void WMIServer::startPlugins()
{
    if (m_infoPlugins.size() < 1)
	return;
    m_tid.resize(m_infoPlugins.size());
    for (unsigned int i = 0; i < m_infoPlugins.size(); i++)
    {
	pthread_create(&m_tid[i], NULL, start_plugin, (void *) (&m_infoPlugins[i]));
    }
}

void WMIServer::stopPlugins()
{
    STOP = true;
    ERS_LOG("Stopping plugins...");
    for (unsigned int i = 0; i < m_tid.size(); i++)
    {
	(void) pthread_join(m_tid[i], NULL);
    }
}
