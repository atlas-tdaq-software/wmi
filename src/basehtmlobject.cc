#include "wmi/basehtmlobject.h"

using namespace daq::wmi;

std::ostream& daq::wmi::operator<<(std::ostream& str, const BaseHTMLObject& param)
{
    return param.write(str);
}

void BaseHTMLObject::setUseFont(bool uFont)
{
    m_useFont = uFont;
}

void BaseHTMLObject::setBGColor(const Color &bColor)
{
    m_bgcolor = bColor;
}

void BaseHTMLObject::setFont(const Font& ft)
{
    m_font = ft;
    m_useFont = true;
}

void BaseHTMLObject::setPluginName(const std::string & pluginName)
{
    m_pluginName = pluginName;
}

void BaseHTMLObject::setOutputDir(const std::string & outputDir)
{
    m_outputDir = outputDir;
}

Color& BaseHTMLObject::getBGColor()
{
    return m_bgcolor;
}

BaseHTMLObject::~BaseHTMLObject()
{
}

std::string BaseHTMLObject::replaceSpecialSymbol(const std::string &str)
{
    std::string st = str;
    for (unsigned int i = 0; i < st.size(); i++)
    {
	if (st[i] == '#' || st[i] == '<' || st[i] == '>' || st[i] == '"')
	    st[i] = '_';
    }
    return st;
}
