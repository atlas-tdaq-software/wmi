#include "wmi/hr.h"

using namespace daq::wmi;

HR::HR()
{
    m_font = Font();
    m_bgcolor = Color(false);
}

HR::~HR()
{
}

std::ostream& HR::write(std::ostream& str) const
{
    str << "<hr />\n";
    return str;
}

