#include "wmi/utils.h"

using namespace daq::wmi;

std::string Utils::toString(int i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}

std::string Utils::toString(unsigned int i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}

std::string Utils::toString(float i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}

std::string Utils::toString(double i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}

std::string Utils::toString(long i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}

