#include "wmi/text.h"

using namespace daq::wmi;

TextOption::TextOption()
{
    m_underline = false;
    m_bold = false;
    m_italic = false;
    m_subscripe = false;
    m_small = false;
}

TextOption::TextOption(const std::string & _opt)
{
    std::string opt = _opt;
    m_underline = false;
    m_bold = false;
    m_italic = false;
    m_subscripe = false;
    m_small = false;
    if (opt.size() > 0)
    {
	int i = opt.find(",");
	while (i != -1)
	{
	    std::string st = opt.substr(0, i);
	    setValue(st);
	    opt.erase(opt.begin(), opt.begin() + i + 1);
	    i = opt.find(",");
	}
	setValue(opt);
    }
}

void TextOption::setValue(const std::string & val)
{
    if (val.size() > 0)
    {
	if (val.compare("b") == 0)
	    m_bold = true;
	else if (val.compare("u") == 0)
	    m_underline = true;
	else if (val.compare("i") == 0)
	    m_italic = true;
	else if (val.compare("sub") == 0)
	    m_subscripe = true;
	else if (val.compare("small") == 0)
	    m_small = true;
    }
}
Text::Text(const std::string & value, const TextOption & opt)
{
    m_useFont = false;
    m_text = value;
    m_bgcolor = Color(false);
    m_opt = opt;
}

Text::Text(const std::string & value, unsigned int size, const TextOption & opt)
{
    if (size > 0)
    {
	m_useFont = true;
	m_font = Font();
	m_font.setSize(size);
    }
    else
    m_useFont = false;
    m_text = value;
    m_bgcolor = Color(false);
    m_opt = opt;
}

Text::Text(const std::string & value, unsigned int size, const Color & col,
    const TextOption & opt)
{
    m_useFont = true;
    m_font = Font();
    m_text = value;
    m_bgcolor = Color(false);
    m_font.setSize(size);
    m_font.setColor(col);
    m_opt = opt;
}

Text::~Text()
{
}

std::ostream& Text::write(std::ostream& str) const
{
    if (m_useFont)
	str << m_font.writeStart();
    if (m_opt.m_underline)
	str << "<u>";
    if (m_opt.m_bold)
	str << "<b>";
    if (m_opt.m_italic)
	str << "<i>";
    if (m_opt.m_subscripe)
	str << "<sub>";
    if (m_opt.m_small)
	str << "<small>";
    str << m_text;
    if (m_opt.m_underline)
	str << "</u>";
    if (m_opt.m_bold)
	str << "</b>";
    if (m_opt.m_italic)
	str << "</i>";
    if (m_opt.m_subscripe)
	str << "</sub>";
    if (m_opt.m_small)
	str << "</small>";
    if (m_useFont)
	str << m_font.writeEnd();
    return str;
}

