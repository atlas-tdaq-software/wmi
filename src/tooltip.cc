#include "wmi/tooltip.h"

using namespace daq::wmi;

Tooltip::Tooltip(const std::string & text)
: m_text(text)
{
    m_font = Font();
    m_bgcolor = Color(false);
}

Tooltip::~Tooltip()
{
}

std::ostream& Tooltip::write(std::ostream& str) const
{
    std::string first_line = m_text.substr(0, m_text.find('\n'));

    if (first_line != m_text) {
        first_line += "...<sub style=\"color:blue\">&#8628;</sub>";
        str << "<div class=\"tooltip\" tabindex=\"1\">" << first_line <<
                "</div><div class=\"tooltiptext\">"
                << m_text.substr(m_text.find('\n') + 1) << "</div>";
    } else {
        str << "<div>" << m_text << "</div>";
    }
    return str;
}

