#include <sys/stat.h>

#include <boost/regex.hpp>

#include <wmi/htmloutputstream.h>

using namespace daq::wmi;

HTMLOutputStream::HTMLOutputStream(unsigned int period)
{
    m_period = period;
    m_currentIndex = -1;
    m_maxIndex = 0;
    m_isOpened = false;
    m_pageName = "WMI";
    m_bgColor = "fcfcfc";
    m_endFile = "</body>\n</html>";
}

HTMLOutputStream::~HTMLOutputStream()
{
}

std::string int2String(unsigned int i)
{
    std::ostringstream out;
    out << i;
    return out.str();
}

std::string HTMLOutputStream::getBeginFile()
{
    std::string beginFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    beginFile +=
	"<!DOCTYPE html SYSTEM \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
    beginFile += "<html><head><title>" + m_pageName
	+ "</title><meta http-equiv=\"REFRESH\" content=\"";
    beginFile += int2String(m_period) + "\" />\n";
    beginFile += "</head>\n<body bgcolor=\"#" + m_bgColor + "\">\n";
    return beginFile;
}

int HTMLOutputStream::open()
{
    return open("/index.html", "");
}

int HTMLOutputStream::open(const std::string &_fileName)
{
    return open(_fileName, "");
}

int HTMLOutputStream::open(const std::string & _fileName, const std::string & dirName)
{
    std::string fileName = _fileName;
    fileName = BaseHTMLObject::replaceSpecialSymbol(fileName);
    int ind = getFileDescriptor(dirName + "/" + fileName);
    if (ind == -1)
    {
	ind = getNewIndex();
	if (ind == -1)
	    return -1;
	m_index_map.insert(
	    std::map<std::string, int>::value_type(dirName + "/" + fileName, ind));
	std::string plName; // = m_pluginName;
	if (m_pluginName.size() > 0)
	{
	    mkdir((m_outputDir + m_pluginName + "_new").c_str(),
		S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
	    plName = m_pluginName + "_new/";
	    if (dirName.size() > 0)
	    {
		mkdir((m_outputDir + plName + dirName).c_str(),
		    S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
		plName += dirName + "/";
	    }
	}
	else
	{
	    plName = "";
	    fileName += "_new";
	}
	std::string f = m_outputDir + plName + fileName;
	std::ostream * file = 0;
	if (!m_useScript)
	{
	    file = new std::ofstream(f.c_str());
	    (*file) << getBeginFile();
	}
	else
	{
	    file = new std::ostringstream();
	    m_file_description_map.insert(
		std::map<int, void*>::value_type(ind,
		    (void*) new FileDescription(m_scriptDir)));
	}
	m_currentIndex = ind;
	m_file_map.insert(std::map<int, void*>::value_type(ind, (void*) file));
    }
    else
    m_currentIndex = ind;
    return ind;
}

void HTMLOutputStream::close(int ind)
{
    std::map<int, void*>::iterator myIterator;
    myIterator = m_file_map.find(ind);
    if (myIterator != m_file_map.end())
    {
	time_t temptime;
	time(&temptime);
	std::string st = "<font size=\"2\" color=\"#555555\"><sub>Page layout has been generated at ";
	st += ctime(&temptime);
	st += "</sub></font>\n";
	std::ofstream* file = (std::ofstream*) (*myIterator).second;
	(*file) << "<br />" << st;
	(*file) << m_endFile;
	if (m_useScript)
	{
	    writeWithScript(ind);
	}
	else
	{
	    (*file).close();
	}
	delete file;
	m_file_map.erase(ind);
	deleteIndexFromMap(ind);
	if (m_useScript)
	{
	    myIterator = m_file_description_map.find(ind);
	    if (myIterator != m_file_description_map.end())
	    {
		FileDescription* fd = (FileDescription*) (*myIterator).second;
		delete fd;
		m_file_description_map.erase(myIterator);
	    }
	}
    }
}

void HTMLOutputStream::closeAll()
{
    if (m_file_map.empty())
	return;
    std::map<int, void*>::iterator myIterator;
    myIterator = m_file_map.begin();
    while (myIterator != m_file_map.end())
    {
	close((*myIterator).first);
	myIterator++;
    }
    m_file_map.clear();
    m_index_map.clear();
    if (m_useScript)
    {
	myIterator = m_file_description_map.begin();
	while (myIterator != m_file_description_map.end())
	{
	    FileDescription* fd = (FileDescription*) (*myIterator).second;
	    delete fd;
	    myIterator++;
	}
	m_file_description_map.clear();
    }
    m_maxIndex = 0;
    m_currentIndex = 0;
}

void HTMLOutputStream::writeWithScript(int ind)
{
    FileDescription* fd = getFileDescription(ind);
    if (fd == NULL)
	return;
    std::string f_name = getFileName(ind);
    if (f_name.length() < 1)
	return;
    std::string plName;
    if (m_pluginName.size() > 0)
    {
	plName = m_pluginName + "_new/";
    }
    else
    {
	plName = "";
	f_name += "_new";
    }
    std::string f = m_outputDir + plName + f_name;
    std::ofstream file;
    file.open(f.c_str());
    std::string beginFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    beginFile +=
	"<!DOCTYPE html SYSTEM \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
    beginFile += "<html><head><title>" + m_pageName
	+ "</title><meta http-equiv=\"REFRESH\" content=\"";
    beginFile += int2String(m_period) + "\" />\n";
    (file) << beginFile;
    std::vector<HrefFile> href_file = fd->getHrefFiles();
    for (unsigned int i = 0; i < href_file.size(); i++)
    {
	HrefFile hr = href_file[i];
	std::string f_script = "<link rel=\"" + hr.rel + "\"type=\"" + hr.type
	    + "\" href=\"" + m_scriptDir;
	if (hr.href[0] != '/')
	    f_script += "/";
	f_script += hr.href + "\" />\n";
	file << f_script;
    }

    std::vector < std::string > scr_file = fd->getScriptFiles();
    for (unsigned int i = 0; i < scr_file.size(); i++)
    {
	std::string f_script = "<script src=\"" + m_scriptDir;

	std::string datapath(m_scriptDir);
	boost::regex regexp("http.*:/*[^/]*(/.*)");
	boost::smatch matches;
	if (boost::regex_match(m_scriptDir, matches, regexp) && matches.size() > 1)
	{
	    datapath = std::string(matches[1].first, matches[1].second);
	}
	f_script += scr_file[i] + "\" data-path='" + datapath
	    + "' type='text/javascript'></script>\n";
	file << f_script;
    }
    if (fd->m_variables.size())
    {
	file << "<script type=\"text/javascript\">\n";
	for (unsigned int i = 0; i < fd->m_variables.size(); i++)
	{
	    file << "var " + fd->m_variables[i] + " = null;\n";
	}
	file << "</script>\n";
    }
    file << "</head>\n";
    beginFile = "<body bgcolor=\"#" + m_bgColor + "\">\n";
    file << beginFile;
    std::map<int, void*>::iterator it = m_file_map.find(ind);
    if (it != m_file_map.end())
    {
	std::ostringstream * out = (std::ostringstream*) (it->second);
	file << out->str();
    }

    file.close();
}

bool HTMLOutputStream::writeFramePage(const std::string & fileName,
    const FrameSet& frameset, const std::string & dirName)
{
    std::string f_name = "";
    if (dirName.size() > 0)
    {
	f_name = dirName;
	if (dirName[dirName.size()] != '\\' || dirName[dirName.size()] != '/')
	    f_name += "/";
    }
    else
    {
	f_name += "/";
    }
    f_name += fileName;

    std::string plName;
    if (m_pluginName.size() > 0)
    {
	plName = m_pluginName + "_new/";
    }
    else
    {
	plName = "";
	f_name += "_new";
    }
    std::string f = m_outputDir + plName + f_name;
    std::ofstream file;
    file.open(f.c_str());
    std::string beginFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    beginFile +=
	"<!DOCTYPE html SYSTEM \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    beginFile += "<html><head><title>" + m_pageName
	+ "</title><meta http-equiv=\"REFRESH\" content=\"";
    beginFile += int2String(m_period) + "\" />\n";
    (file) << beginFile; //</HEAD>\n";
    (file) << "</head>\n";
    (file) << frameset;
    (file) << "</html>\n";
    file.close();
    return true;
}

void HTMLOutputStream::write(BaseHTMLObject& tb, int ind)
{
    std::map<int, void*>::iterator myIterator;
    myIterator = m_file_map.find(ind);
    if (myIterator != m_file_map.end())
    {
	tb.setPluginName(m_pluginName);
	tb.setOutputDir(m_outputDir);
	tb.m_fd = (getFileDescription(ind));
	std::ofstream* file = (std::ofstream*) (*myIterator).second;
	(*file) << tb;
    }
}

