#include "wmi/font.h"
#include <sstream>

using namespace daq::wmi;

std::string getHexString(unsigned int i)
{
    std::ostringstream out;
    if (i < 17)
	out << "0";
    out << std::hex << i;
    return out.str();
}

Color::Color()
{
    m_color_hex = ""; //"000000";
}

Color::Color(unsigned int r, unsigned int g, unsigned int b)
{
    if (r <= 255 && g <= 255 && b <= 255)
    {
	m_color_hex = getHexString(r);
	m_color_hex += getHexString(g);
	m_color_hex += getHexString(b);
    }
    else
    m_color_hex = "000000";
}

Color::Color(bool isExist)
{
    if (isExist)
	m_color_hex = "000000";
    else
    m_color_hex = "";
}

std::string Color::write()
{
    std::string st = "";
    if (m_color_hex.size() > 0)
	st = "color=\"#" + m_color_hex + "\" ";
    return st;
}

std::string Color::getColor() const
{
    std::string st = m_color_hex;
    return st;
}

Font::Font()
{
    m_used = false;
    m_size = 3;
    m_color = Color();
}

Font::Font(unsigned int sz, const Color& cl)
{
    m_size = sz;
    m_color = cl;
    m_used = true;
}

Font::Font(unsigned int sz)
{
    m_size = sz;
    m_color = Color();
    m_used = true;
}

Font::~Font()
{
}

void Font::setSize(unsigned int sz)
{
    m_used = true;
    if (sz > 7)
	m_size = 7;
    else
    m_size = sz;
}

void Font::setColor(const Color& cl)
{
    m_used = true;
    m_color = cl;
}

std::string Font::writeStart() const
{
    std::string st = "";
    if (m_used)
    {
	if (m_size > 0)
	{
	    std::ostringstream out;
	    out << m_size;
	    st = "<font size=\"" + out.str() + "\" color=\"#" + m_color.getColor()
		+ "\">";
	}
	else
	st = "<font color=\"#" + m_color.getColor() + "\">";
    }
    return st;
}

std::string Font::writeEnd() const
{
    std::string st = "";
    if (m_used)
	st = "</font>\n";
    return st;
}

