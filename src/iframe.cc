#include "wmi/iframe.h"

using namespace daq::wmi;

IFrame::IFrame(const std::string & src, const std::string & name, unsigned int width,
    bool isWPersent, unsigned int height, bool isHPersent)
{
    m_src = src;
    m_name = name;
    m_scroll = NODEFINE;
    m_border = false;
    m_aling = NODEFINE_ALING;
    m_hspace = 0;
    m_vspace = 0;
    m_width = Utils::toString(width);
    if (isWPersent)
	m_width += "%";
    m_height = Utils::toString(height);
    if (isHPersent)
	m_height += "%";
}

IFrame::~IFrame()
{
}

void IFrame::setIndentHorizontal(unsigned int hspace)
{
    m_hspace = hspace;
}

void IFrame::setIndentVertical(unsigned int vspace)
{
    m_vspace = vspace;
}

void IFrame::setScroll(Scrolling scroll)
{
    m_scroll = scroll;
}

void IFrame::setAling(Aling aling)
{
    m_aling = aling;
}

void IFrame::setFrameBorder(bool border)
{
    m_border = border;
}

std::ostream& IFrame::write(std::ostream& str) const
{
//std::ostream& daq::wmi::operator<< (std::ostream& str, Frame& param){
    str << "<iframe src=\"" << m_src << "\" ";
    if (m_name.size() > 0)
	str << "name=\"" << m_name << "\" ";
    str << "width=\"" << m_width << "\" height=\"" << m_height << "\" ";
    if (m_hspace > 0)
	str << "hspace=\"" << m_hspace << "\" ";
    if (m_vspace > 0)
	str << "vspace=\"" << m_vspace << "\" ";
    if (m_scroll != NODEFINE)
    {
	str << "scrolling=\"";
	if (m_scroll == YES)
	    str << "yes";
	else if (m_scroll == NO)
	    str << "no";
	else
	str << "auto";
	str << "\" ";
    }
    if (m_aling != NODEFINE_ALING)
    {
	str << "align=\"";
	if (m_aling == ABSMIDDLE)
	    str << "absmiddle";
	else if (m_aling == TOP)
	    str << "top";
	else if (m_aling == BASELINE)
	    str << "baseline";
	else if (m_aling == BOTTOM)
	    str << "bottom";
	else if (m_aling == LEFT)
	    str << "left";
	else if (m_aling == MIDDLE)
	    str << "middle";
	else if (m_aling == RIGHT)
	    str << "right";
	else if (m_aling == TEXTTOP)
	    str << "texttop";
	str << "\" ";
    }
    if (m_border)
	str << "frameborder=\"yes\"";
    str << ">\n";
    str << "</iframe>\n";

    return str;
}

