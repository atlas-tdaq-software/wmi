#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "wmi/picture.h"

using namespace daq::wmi;

Picture::Picture(const std::string & ln)
{
    m_link = ln;
    m_width = 100;
    m_height = 100;
    m_length = 0;
    m_needCopy = false;
    m_bgcolor = Color(false);
}

Picture::Picture(const std::string & ln, unsigned int w, unsigned int h)
{
    m_link = ln;
    m_width = w;
    m_height = h;
    m_length = 0;
    m_needCopy = false;
    m_bgcolor = Color(false);
}

Picture::Picture(long l, unsigned char data[], const std::string & ln)
{
    m_length = l;
    m_picture_data = new unsigned char[m_length]; //data;
    for (long i = 0; i < m_length; i++)
    {
	m_picture_data[i] = data[i];
    }
    m_link = ln;
    m_width = 100;
    m_height = 100;
    m_needCopy = true;
    m_bgcolor = Color(false);
}

Picture::~Picture()
{
    if (m_length > 0)
	delete[] m_picture_data;
}

void Picture::setNeedCopy(bool needCopy)
{
    m_needCopy = needCopy;
}

void Picture::setSize(unsigned int w, unsigned int h)
{
    m_width = w;
    m_height = h;
}

namespace
{
    int copy_file(const char * in_name, const char * out_name)
    {
	int ret = 1;

	FILE * in = ::fopen(in_name, "r");
	if (!in)
	{
	    return ret;
	}

	FILE * out = ::fopen(out_name, "w");
	if (out)
	{
	    int c;
	    while ((c = ::getc(in)) != EOF)
		::putc(c, out);
	    ::fclose(out);
	    ret = 0;
	}

	::fclose(in);

	return ret;
    }
}

std::ostream &
Picture::write(std::ostream& str) const
    {
    std::string file = m_link;

    if (m_needCopy)
    {
	unsigned long i = m_link.find_last_of("/");

	if (i != std::string::npos && i < (m_link.size() - 1))
	{
	    file = m_link.substr(i + 1);
	}

	std::string new_name = m_outputDir + m_pluginName + "_new/" + file;
	if (copy_file(m_link.c_str(), new_name.c_str()))
	{
	    ERS_LOG(
		"Error copying '" << m_link << "' to '" << new_name << "' file : "
		    << strerror(errno));
	}
    }

    if (m_link.size() > 4 && !::strcasecmp("svg", m_link.c_str() + m_link.size() - 3))
    {
	str << "<object data=\"" << file << "\" type=\"image/svg+xml\" width=\""
	    << m_width << "\" height=\"" << m_height << "\">\n</object>\n";
	if (m_fd)
	    m_fd->addScriptFile("svg.js");
    }
    else
    {
	str << "<img src=\"" << file << "\" width=\"" << m_width << "\" height=\""
	    << m_height << "\" /><br />\n";
    }

    return str;
}

