#include "wmi/frameset.h"

using namespace daq::wmi;

FrameSet::FrameSet(unsigned int lenghtColumn, UNIT unitColumn, unsigned int lenghtRow,
    UNIT unitRow)
{
    m_value_column = "";
    m_value_row = "";
    m_isBorder = false;
    m_columns = 1;
    m_lines = 1;
    m_border = -1;
    m_isBorder = false;
    m_spacing = -1;
    m_canAddColumn = true;
    m_border_color = Color(false);

    m_frames.resize(1);
    AddStringValue2ColRow(unitColumn, lenghtColumn, false);
    AddStringValue2ColRow(unitRow, lenghtRow, true);
}

FrameSet::~FrameSet()
{
    for (unsigned int i = 0; i < m_lines * m_columns; i++)
    {
	if (m_frames[i])
	    delete (m_frames[i]);
    }
}

void FrameSet::AddStringValue2ColRow(UNIT unitVal, unsigned int lenghtVal, bool isRow)
{
    if (isRow)
    {
	if (m_value_row.size() > 0)
	    m_value_row += ",";
	if (unitVal == ALL_FREE)
	{
	    m_value_row += "*";
	}
	else
	{
	    m_value_row += Utils::toString(lenghtVal);
	    if (unitVal == PERCENT)
		m_value_row += "%";
	}
    }
    else
    {
	if (m_value_column.size() > 0)
	    m_value_column += ",";
	if (unitVal == ALL_FREE)
	{
	    m_value_column += "*";
	}
	else
	{
	    m_value_column += Utils::toString(lenghtVal);
	    if (unitVal == PERCENT)
		m_value_column += "%";
	}
    }
}

void FrameSet::addRow(unsigned int lenghtRow, UNIT unitRow)
{
    m_lines++;
    m_frames.resize(m_columns * m_lines);
    m_canAddColumn = false;
    AddStringValue2ColRow(unitRow, lenghtRow, true);
}

void FrameSet::setBorder(unsigned int br)
{
    m_border = (int) br;
}

bool FrameSet::addColumn(unsigned int lenghtColumn, UNIT unitColumn)
{
    if (!m_canAddColumn)
	return false;
    m_columns++;
    m_frames.resize(m_columns * m_lines);
    AddStringValue2ColRow(unitColumn, lenghtColumn, false);
    return true;
}

bool FrameSet::addElement(unsigned int i, unsigned int j, FrameBase* Obj)
{
    if (i > 0)
	i--;
    if (j > 0)
	j--;
    if ((i <= m_columns) && (j <= m_lines))
    {
	m_frames[i + j * m_columns] = Obj;
	return true;
    }
    else
    return false;
}

void FrameSet::setFrameBorder(bool border)
{
    m_isBorder = border;
}

void FrameSet::setFrameSpacing(unsigned int spacingsize)
{
    m_spacing = (int) spacingsize;
}

std::ostream& FrameSet::write(std::ostream& str) const
{
    str << "<frameset ";
    if (m_border > -1)
    {
	str << "border=\"" << m_border << "\" ";
	str << "frameborder=\"Yes\" ";
    }
    else if (m_isBorder)
	str << "frameborder=\"Yes\" ";
    else
    str << "frameborder=\"No\" ";
    if (m_spacing > -1)
	str << "framespacing=\"" << m_spacing << "\" ";
    if (m_border_color.getColor().size() > 0)
	str << "bordercolor=\"#" << m_border_color.getColor() << "\" ";
    str << "cols=\"" << m_value_column << "\" rows=\"" << m_value_row << "\">\n";

    for (unsigned int j = 0; j < m_lines; j++)
    {
	for (unsigned int i = 0; i < m_columns; i++)
	{
	    str << (*m_frames[i + j * m_columns]);
	}
    }
    str << "</frameset>";
    return str;
}

