#include "wmi/timegraph.h"

using namespace daq::wmi;

TimeGraph::TimeGraph(const std::string & name, const std::string & yLabel,
    std::vector<std::pair<unsigned long, float> > poits, unsigned int width,
    unsigned int height, std::pair<float, float> min_max_y)
{
    m_poits = poits;
    m_style = 0;
    m_color = 0;
    m_height = Utils::toString(height);
    m_width = Utils::toString(width);
    m_fd = NULL;
    m_name = name;
    m_yLabel = yLabel;
}

TimeGraph::TimeGraph(const std::string & name, const std::string & yLabel,
    std::vector<std::pair<unsigned long, float> > poits)
{
    m_poits = poits;
    m_style = 0;
    m_color = 0;
    m_height = "100%";
    m_width = "100%";
    m_fd = NULL;
    m_name = name;
    m_yLabel = yLabel;
}

TimeGraph::TimeGraph(const std::string & name, const std::string & yLabel,
    std::vector<std::pair<unsigned long, float> > poits, unsigned int width,
    bool isWPercent, unsigned int height, bool isHPercent)
{
    m_poits = poits;
    m_style = 0;
    m_color = 0;
    m_height = Utils::toString(height);
    if (isHPercent)
	m_height += "%";
    m_width = Utils::toString(width);
    if (isWPercent)
	m_width += "%";
    m_fd = NULL;
    m_name = name;
    m_yLabel = yLabel;
}

std::ostream& TimeGraph::write(std::ostream& str) const
    {
    str << "<object classid=\"clsid:CAFEEFAC-0016-0000-0000-ABCDEFFEDCBA\" name="
	<< m_name << " id=" << m_name << " width=";
    str << m_width << " height=" << m_height
	<< " codebase=\"http://java.sun.com/javase/downloads/ea.jsp\">\n";
    str << "<param name=\"type\" value=\"application/x-java-applet;version=1.5\">\n";
    str << "<param name=\"code\" value=\"wmi.plot.wmiplot.class\">\n";
    str << "<param name=\"codebase\" value=\"" << m_fd->getHTTPRoot() + "/applet"
	<< "\">\n";
    str << "<param name=\"archive\" value=\"plot.jar\">\n";
    str << "<param name=\"title\" value=\"" << m_name << "\">\n";
    if (m_bgcolor.getColor().size() > 0)
	str << "<param name=\"bgcolor\" value=\"" << m_bgcolor.getColor() << "\">\n";
    if (m_yLabel.size() > 0)
	str << "<param name=\"ylabel\" value=\"" << m_yLabel << "\">\n";
    str << "<param name=\"points\" value=\"" << m_poits.size() << " ";
    for (unsigned int i = 0; i < m_poits.size(); i++)
    {
	unsigned long long tim = m_poits[i].first;
	str << tim << ";" << m_poits[i].second << " ";
    }
    str << "\">\n";
    str << "<comment>\n";
    str
	<< "<embed type=\"application/x-java-applet;version=1.5\" code=wmi.plot.wmiplot.class java_CODE=wmi.plot.wmiplot.class ";
    str << "width=" << m_width << " height=" << m_height
	<< " pluginspage=\"http://java.sun.com/javase/downloads/ea.jsp\"";
    str << "codebase='" << m_fd->getHTTPRoot() + "/applet" << "' archive='plot.jar'";
    str << " title=\"" << m_name << "\"";
    if (m_bgcolor.getColor().size() > 0)
	str << " bgcolor=\"" << m_bgcolor.getColor() << "\"";
    if (m_yLabel.size() > 0)
	str << " ylabel=\"" << m_yLabel << "\"";
    str << " points=\"" << m_poits.size() << " ";
    for (unsigned int i = 0; i < m_poits.size(); i++)
    {
	unsigned long long tim = m_poits[i].first;
	str << tim << ";" << m_poits[i].second << " ";
    }
    str << "\"/>\n</comment>\n";
    str << "</object>\n";

    return str;
}

