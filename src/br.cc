#include "wmi/br.h"

using namespace daq::wmi;

BR::BR()
{
    m_font = Font();
    m_bgcolor = Color(false);
}

BR::~BR()
{
}

std::ostream& BR::write(std::ostream& str) const
{
    str << "<br />\n";
    return str;
}

