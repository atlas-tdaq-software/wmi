#ifndef _WMI_FRAME_INCLUDED
#define _WMI_FRAME_INCLUDED
/*! \file frame.h Defines the frame class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */


#include <iostream>
#include "wmi/font.h"
#include "wmi/utils.h"
//#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Define the base class of Frame and FrameSet.
 */
class FrameBase{
protected:
	Color m_border_color;
public:
    /*! Virtual function for write the object to stream.
        \param str The stream for write object.
     */
        virtual std::ostream& write(std::ostream& str) const = 0;
    /*! Destructor of object. If need delete font and background color of the object.
     */
        virtual ~FrameBase(){};
    /*! Set the border color. 
        \param bColor Border color.
        \sa Color()
     */
        void setBorderColor(const Color &bColor) {m_border_color = bColor;};
 	

};

/*! \brief  Define scrolling enumeration.
 */
//enum Scrolling {YES,NO,AUTO,NODEFINE};

/*! \brief  Define the Frame object of html. This object can be added only to FrameSet object.
 */
class Frame:public FrameBase{
private:
	std::string m_src;
	std::string m_name;
        int m_margin_width;
	int m_margin_height;
	Scrolling m_scroll;
	bool m_noresize;
	bool m_border;
public:
    /*! Constructor.
	\param src URL of HTML page.
	\param name Name of frame. May be need for link from other documents using attribut taget.
    */
        Frame(const std::string & src,const std::string & name="");
    /*! Destructor.
    */
        ~Frame();
    /*! Set margin width (left and right) in pixels. If this parameter not set, brouser automatic set optimal value.
	\param width Indent of frame in pixels.
    */
	void setMarginWidth(unsigned int width);
    /*! Set margin height (top and bottom) in pixels. If this parameter not set, brouser automatic set optimal value.
        \param height Indent of frame in pixels.
    */
        void setMarginHeight(unsigned int height);
    /*! Set scrolling of frame
	\param scroll Enum of Scrolling. 
    */	
	void setScroll(Scrolling scroll);
    /*! Set noresize mode of frame
    */
	void setNoResize();
    /*! Set frame border.
	\param border If true border exists.
    */
        void setFrameBorder(bool border);

    /*! Write frame object to stream in html format. This method are used automatically output.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;

};

    /*! \fn Realization operator <<.
        \param str The stream for write object.
        \param param The object which need to write.
     */
     std::ostream& operator<< (std::ostream& str, const FrameBase& param);

}//end wmi namespace
}//end daq namespace

#endif

