#ifndef _UTILS_INCLUDED
#define _UTILS_INCLUDED
/*! \file utils.h Defines the utils class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <sstream>
#include <string>



namespace daq{

namespace wmi{

/*! \brief  Define scrolling enumeration.
 */
enum Scrolling {YES,NO,AUTO,NODEFINE};

/*! \brief  Define some static methods which can be used in different classes.
 */

class Utils{
public:
/*!     Convert integer to string.
	\param i Value of integer.
*/
	static std::string toString(int i);
/*!     Convert unsigned integer to string.
        \param i Value of unsigned integer.
*/
	static std::string toString(unsigned int i);

/*!     Convert float to string.
        \param i Value of float.
*/
	static std::string toString(float i);

/*!     Convert double to string.
        \param i Value of double.
*/
	static std::string toString(double i);

/*!     Convert long to string.
        \param i Value of long.
*/
	static std::string toString(long i);

};

}//end wmi namespace
}//end daq namespace

#endif

