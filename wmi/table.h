#ifndef _WMI_TABLE_INCLUDED
#define _WMI_TABLE_INCLUDED
/*! \file table.h Defines the Table class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include <utility>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

class Table;

class CellOption{
	friend class Table;
private:
	std::string m_width;
	std::string m_align;
	std::string m_valign;
	std::string m_height;
	Color m_bgcolor;	
public:
	CellOption();
};

typedef std::pair< std::vector< BaseHTMLObject* >, CellOption > CELL; 
/*! \brief  Define the table object of html. Insert table to the html page. Elements of table is BaseHTMLObject
 */

class Table:public BaseHTMLObject{
private:
        std::vector< CELL > m_htmlObjs;
        unsigned int m_columns;
        unsigned int m_lines;
	int m_border;
	int m_cellspacing;
        int m_cellpadding;
	std::string m_width;
        std::string m_height;
	std::vector<std::string> m_tdWidth;
//	std::string* m_tdWidth;
public:
/*!     Create table object.The size of table on html page. Define size of table lines=1, columns=1.
*/
        Table();
/*!     Create table object.The size of table on html page. 
	\param column	Number columns is in the table.
	\param row	Number rows is in the table. 
*/
        Table(unsigned int column,unsigned int row);
/*!     Destroy table object and all its elements.
*/
        ~Table();
/*!     Set border of table. Default table has not border (border=0)
	\param br	Width in pixels.
*/
	void setBorder(int br);
/*!     Set size between neighbouring cells of table. 
        \param br    Size in pixels.
*/
        void setCellSpacing(int br);
/*!     Set size between frame of cell and data of cell.
        \param br    Size in pixels.
*/
        void setCellPadding(int br);
/*!     Set width of table. Default table has width "100%".
        \param w       Width of table in pixels or percent. 
	\param isPercent If true width define in percent else in pixels.
*/
	void setWidth(unsigned int w,bool isPercent = false);
/*!     Set height of table. Default table has width "100%".
        \param h       Height of table in pixels or percent.
        \param isPercent If true width define in percent else in pixels.
*/
        void setHeight(unsigned int h,bool isPercent = false);
/*!     Set width of column. 
	\param number     Number of column. Begin at 1.
        \param w          Width of table in pixels or percent.
        \param isPercent  If true width define in percent else in pixels.
*/
	void setTDWidth(unsigned int number,unsigned int w,bool isPercent = false);
/*!     Set width of cell.
        \param column     Number of column. Begin at 1.
        \param row       Number of row. Begin at 1.
        \param w          Width of cell in pixels or percent.
        \param isPercent  If true width define in percent else in pixels.
*/
        void setWidthCell(unsigned int column,unsigned int row,unsigned int w,bool isPercent = false);
/*!     Set height of cell.
        \param column     Number of column. Begin at 1.
        \param row       Number of row. Begin at 1.
        \param w          Height of cell in pixels or percent.
        \param isPercent  If true width define in percent else in pixels.
*/
        void setHeightCell(unsigned int column,unsigned int row,unsigned int w,bool isPercent = false);
/*!     Set align of cell.
	\param column     Number of column. Begin at 1.
        \param row       Number of row. Begin at 1.
        \param number     Number of align in the cell.
        * 1 - left;
        * 2 - center;
        * 3 - right;
*/
        void setAlignCell(unsigned int column,unsigned int row,unsigned int number);
/*!     Set vertical align of cell.
        \param column     Number of column. Begin at 1.
        \param row        Number of row. Begin at 1.
        \param number     Number of vertical align in the cell.
	* 1 - top;
	* 2 - bottom;
	* 3 - middle; (default value)
*/
        void setVAlignCell(unsigned int column,unsigned int row,unsigned int number);
/*!     Set vertical align of cell.
        \param column     Number of column. Begin at 1.
        \param row       Number of row. Begin at 1.
        \param col        Background color of this cell.
*/
        void setBGColorCell(unsigned int column,unsigned int row,Color col);
/*!     Add element to table.
        \param i          Number of column. Begin at 1.
        \param j          Number of row. Begin at 1.
        \param Obj        Pointer of element.
*/
        bool addElement(unsigned int i,unsigned int j,BaseHTMLObject* Obj);
/*!     Add one row to end of table. Resize table.
*/
        void addRow();
/*     Delete last row of table. Resize table.
*/
//	void deleteLastLine();
    /*! Write table object to stream in html format.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
};


}//end wmi namespace
}//end daq namespace

#endif

