#ifndef WMI_HTML_OUTPUT_STREAM_INCLUDED
#define WMI_HTML_OUTPUT_STREAM_INCLUDED

/*! \file htmloutputstream.h Defines the HTMLOutputStreame class of wmi.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include "wmi/outputstream.h"
#include <fstream>
#include <time.h>


namespace daq{

namespace wmi{

/*! \brief  This class create html files and write its.
    \sa
 */

class HTMLOutputStream:public OutputStream{
        std::string m_endFile;
	std::ofstream m_file;
//	std::map<int,std::ofstream*> m_file_map;	
private:
	std::string getBeginFile();
//	int getNewIndex();
//	int getIndex(std::string fname);
public:
    /*! Constructor.
	\param period Period of automatic update.
     */
        HTMLOutputStream(unsigned int period);
    /*! Open file with name index.html.
     */
        int open();
    /*! Open file.
        \param fileName The name of file.
        \return Index of file.
     */
        int open(const std::string & fileName);
    /*! Open file.
        \param fileName The name of file.
        \param dirName The name of directory where this file will be created.
        \return Index of file.
     */
	int open(const std::string & fileName,const std::string &dirName);
    /*! Close file.
        \param ind The index of file.
     */
        void close(int ind);
    /*! Write header of file to file.
        \param fileDescriptor The file descriptor where this object need write.
     */
        void writeWithScript(int fileDescriptor);
    /*! Write frame page to file.
        \param fileName The name of file.
        \param frameset The FrameSet object of file which need to create.
        \param dirName The name of directory where this file will be created.
     */
        bool writeFramePage(const std::string & fileName,const FrameSet& frameset,const std::string &dirName="");
    /*! Close all files. Recommend use this methods when plug-in finish the work.
     */
	void closeAll();
    /*! Write object to file.
        \param tb WMI html object.
        \param ind The index of file where this object need write.
     */
        void write(BaseHTMLObject& tb,int ind);
    /*! Destructor.
     */
        ~HTMLOutputStream();
};


}//end wmi namespace
}//end daq namespace

#endif

