#ifndef TEST_PLUGIN_INCLUDED
#define TEST_PLUGIN_INCLUDED

#include "wmi/pluginbase.h"
#include "wmi/htmloutputstream.h"

namespace daq
{
namespace wmi
{

    class TestPlugin: public PluginBase
    {
    public:
	TestPlugin();
	void periodicAction();
	void stop();
	void configure(const ::wmi::InfoParameter& params);
	~TestPlugin();
    };

    extern "C" PluginBase* create()
    {
	return new TestPlugin;
    }

    extern "C" void destroy(PluginBase* p)
    {
	delete p;
    }

} //end wmi namespace
} //end daq namespace

#endif

