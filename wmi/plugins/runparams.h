#ifndef RUN_PARAMS_PLUGIN_INCLUDED
#define RUN_PARAMS_PLUGIN_INCLUDED

#include "wmi/table.h"
#include "wmi/list.h"
#include "wmi/text.h"
#include "wmi/picture.h"
#include "wmi/htmloutputstream.h"
#include "wmi/pluginbase.h"
#include "wmi/link.h"
#include "wmi/br.h"
#include <ipc/core.h>
#include <cmath>

//#include <ipc/partition.h>
namespace daq
{
namespace wmi
{

    class RunParamsPlugin: public PluginBase
    {
    public:
	RunParamsPlugin();
	void periodicAction();
	void stop();
	void configure(const ::wmi::InfoParameter& params);
	~RunParamsPlugin();
    };

    extern "C" PluginBase* create()
    {
	return new RunParamsPlugin;
    }

    extern "C" void destroy(PluginBase* p)
    {
	delete p;
    }

} //end wmi namespace
} //end daq namespace

#endif

