#ifndef DQ_PLUGIN_INCLUDED
#define DQ_PLUGIN_INCLUDED

#include <memory>

#include <ipc/partition.h>
#include <ipc/threadpool.h>

#include <wmi/pluginbase.h>
#include <wmi/table.h>

class TCanvas;
class TEnv;
class Configuration;

namespace dqm_config {
    class AlgorithmConfig;
    class AlgorithmConfigFactory;
    namespace dal {
	class DQRegion;
    }
}

namespace daq{
namespace wmi{
    class DQPlugin:public PluginBase
    {
      public:
	struct VisitorParameter
	{
	    VisitorParameter(   DQPlugin * pl,
				const IPCPartition & p,
				std::shared_ptr<Table> & t,
				unsigned int fd );

	    ~VisitorParameter();
	    
	    bool use_webis() const { return plugin->m_use_webis; }
	    const std::string & webis_prefix() const { return plugin->m_webis_prefix; }

	    DQPlugin *				plugin;
	    const dqm_config::dal::DQRegion *	region;
	    IPCPartition 			partition;
	    std::string 			parent_name;
	    std::string 			base_name;
	    std::shared_ptr<Table> 		parent_table;
	    std::shared_ptr<unsigned int> 	parent_row;
	    int 				fd;
	};

	friend class VisitorParameter;

      public:
	DQPlugin();
	~DQPlugin();

	OutputStream * out() { return m_outStream; }

	void periodicAction();
	void stop();
	void configure( const ::wmi::InfoParameter & params );
	bool stopped() const { return m_stopped; }

      private:
	bool writePartition( const IPCPartition & partition );
	bool regenerateNecessary( std::list<IPCPartition> & lst );

      private:
	typedef std::map<IPCPartition, unsigned int> ActivePartitionsMap;

	bool					m_stopped;
	unsigned int				m_picture_height;
	unsigned int				m_picture_width;
	unsigned int				m_results_mask;
	bool					m_show_histograms;
	bool					m_plot_references;
	bool					m_use_webis;
	std::string				m_partition;
	std::string				m_picture_format;
	std::string				m_base_dir;
	std::string				m_webis_prefix;
	TCanvas *				m_canvas;
	TEnv *					m_env;
	IPCThreadPool				m_threadpool;
	dqm_config::AlgorithmConfigFactory  *	m_aconfig;
	ActivePartitionsMap			m_active_partitions;
    };

    extern "C" PluginBase* create() {
	return new DQPlugin;
    }

    extern "C" void destroy( PluginBase * p ) {
	delete p;
    }
}//end wmi namespace
}//end daq namespace


#endif

