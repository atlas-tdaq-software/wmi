#ifndef WMI_PLUGIN_BASE_INCLUDED
#define WMI_PLUGIN_BASE_INCLUDED
/*! \file pluginbase.h Defines the PluginBase abstract class for all plug-ins of wmi.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include "wmi/wmi.hh"
#include "wmi/outputstream.h"

namespace daq{

namespace wmi{
/*! \brief  Base abstract class for all plug-ins of wmi. All wmi plug-ins must implementation all abstract function of this class.
    \sa
 */

class PluginBase{
protected:
        OutputStream* m_outStream;
	unsigned int m_status;
	/* Default parameter can be used in any plug-in
	*/
	std::map<std::string,std::string> m_params;
public:
    /*! Virtual function for start wmi plug-in.
     */
        virtual void periodicAction() = 0;
    /*! Virtual function for stop wmi plug-in.
     */
        virtual void stop() = 0;
    /* Function for configure wmi plug-in.
	\param params The parameters for plug-in from configuration file.
	\param oStream The pointer of OutputStream.
     */
        void configure(const ::wmi::InfoParameter& params, OutputStream* oStream);
    /*! Virtual function for configure wmi plug-in.
        \param params The parameters for plug-in from configuration file.
        *Struct of parameter
        \code
        *typedef sequence<Parameter>     InfoParameter;
        *struct Parameter{
        *string name;//idl string
        *string value;//idl string
        *}
        \endcode
     */
	virtual void configure(const ::wmi::InfoParameter& params) = 0;
    /*! Destructor. 
     */
        virtual ~PluginBase(){};
    /*! Function return name of this plug-in.
     */
	std::string getPluginName();
	unsigned int getStatus();
	void setStatus(unsigned int status);

};

typedef PluginBase* create_t();
typedef void destroy_t(PluginBase*);


}//end wmi namespace
}//end daq namespace

#endif

