#ifndef _BASE_HTML_OBJECT_INCLUDED
#define _BASE_HTML_OBJECT_INCLUDED

#include <string>
#include <iostream>
#include "wmi/font.h"
#include "ers/ers.h"
#include "wmi/filedescription.h"
/*! \file basehtmlobject.h Defines the BaseHTMLObject abstract class for all html classes of wmi.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

namespace daq{

namespace wmi{

/*! \brief  Base abstract class for all html classes of wmi.
    \sa
 */

class BaseHTMLObject{
friend class HTMLOutputStream;
friend class Table;
protected:
        Font m_font;
        bool m_useFont;
        Color m_bgcolor;
        std::string m_pluginName;
        std::string m_outputDir;
	std::string m_id;
	FileDescription* m_fd;

//	friend std::string Font::writeStart() const;
//	friend std::string Font::writeEnd() const;
public:
    /*! Virtual function for write the object to stream.
	\param str The stream for write object. 
     */
        virtual std::ostream& write(std::ostream& str) const = 0;
    /*! Set name of the plug-in. This function is used by the wmi server.
        \param pluginName Name of the plug-in.
     */
        void setPluginName(const std::string & pluginName);
    /*! Set temp directory which used for storage pages of the plug-in. This function is used by the wmi server.
        \param outputDir The name of temp directory. 
     */
        void setOutputDir(const std::string & outputDir);
    /*! Add or delete font for this object.
        \param isFont If true object use font,otherwise font does not use.
	\sa Font()
     */
        void setUseFont(bool isFont);
    /*! Set the background color. The background color use only if object is table 
     *		or if object insert to table. 
        \param bColor The background color.
	\sa Color()
     */
        void setBGColor(const Color &bColor);
    /*! Set the font of object. 
        \param ft The pointer of font object.
	\sa Font()
     */
        void setFont(const Font& ft);
    /*! Get the background color. 
        \return The pointer of background color.
     */
	Color& getBGColor();
    /*! Destructor of object. If need delete font and background color of the object.
     */
        virtual ~BaseHTMLObject();
    /*! Replase special symbol of HTML to symbol "_".
	\param str Old string.
        \return New string.
     */
	static std::string replaceSpecialSymbol(const std::string & str);
	
};
    /*! \fn Realization operator <<.
        \param str The stream for write object.
	\param param The object which need to write.
     */
std::ostream& operator<< (std::ostream& str, const BaseHTMLObject& param);

}//end wmi namespace
}//end daq namespace

#endif

