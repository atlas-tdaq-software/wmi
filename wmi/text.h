#ifndef _WMI_TEXT_INCLUDED
#define _WMI_TEXT_INCLUDED
/*! \file text.h Defines the Text class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

class Text;
/*! \brief  Define option(format) for text.
*/
class TextOption{
friend class Text;
private:
	bool m_underline;
	bool m_bold;
	bool m_italic;
	bool m_subscripe;
	bool m_small;

	TextOption();
	void setValue(const std::string & val);
public:
//	TextOption();
/*!      Create text option(format).
        \param opt String which contained all option. Separator between option is ",".
	* Current options:
	* "u" - underline;
	* "b" - bold;
	* "i" - italic;
	* "sub" - subscripe;
	* "small" - size of font -1;
	*
	* Example: TextOption("u,sub"); 
*/
	TextOption(const std::string & opt);
};

/*! \brief  Define text object of html. Insert text to html page.
*/
class Text: public BaseHTMLObject{
private:
        std::string m_text;
	TextOption m_opt;
//        friend std::string Font::writeStart() const;
//        friend std::string Font::writeEnd() const;

public:
/*!     Create text object without font. Font may be add later.
        \param value String which will be insert in html page.
        \param opt Define option for this text.
*/
        Text(const std::string & value,const TextOption & opt = TextOption());
/*!     Create text object without font. Font may be add later.
        \param value String which will be insert in html page.
	\param size Size of font for this text. Size can has following value: "1..7". If size is 0 font not used. 
        \param opt Define option for this text.
*/
        Text(const std::string & value,unsigned int size ,const TextOption & opt = TextOption());
/*!     Create text object with font.
        \param value String which will be insert in html page.
        \param size Size of font for this text. Size can has following value: "1..7".
	\param col Color of font for this text.
        \param opt Define option for this text.
*/
        Text(const std::string & value,unsigned int size,const Color &col,const TextOption &opt = TextOption());
/*     Create text object without font but define background color. Font may be add later.
        \param value String which will be insert in html page.
        \param bgcol Color of background for this text. Background color used only if this text add to table. 
        \param opt Define option for this text.
*/
//        Text(std::string value, Color bgcol,TextOption opt = TextOption(""));

/*!     Destroy text object.
*/
        ~Text();
    /*! Write text object to stream in html format.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
};


}//end wmi namespace
}//end daq namespace

#endif


