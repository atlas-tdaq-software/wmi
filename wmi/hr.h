#ifndef _WMI_HR_INCLUDED
#define _WMI_HR_INCLUDED
/*! \file hr.h Defines the hr class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */


#include <iostream>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Define the hr object of html. Inserts into the html page a horizontal dividing line.  
 */
class HR: public BaseHTMLObject{
public:
    /*! Empty constructor.
    */
        HR();
    /*! Destructor.
    */
        ~HR();
    /*! Write hr object to stream in html format. This method are used automatically output.  
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;

};


}//end wmi namespace
}//end daq namespace

#endif

