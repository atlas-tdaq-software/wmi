#ifndef _WMI_LIST_INCLUDED
#define _WMI_LIST_INCLUDED
/*! \file list.h Defines the list class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include <vector>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Define the list object of html. Elements of list is string.  
 */
class List: public BaseHTMLObject{
        std::vector<std::string> m_list;
	int m_order;
public:
/*!      Constructor. 
	\param order  Order is a style of numeration and can have following value:
	* 0 - without order
	* 1 - "A" capital letters
	* 2 - "a" lowercase letters
	* 3 - "I" capital Roman numerals
	* 4 - "i" lowercase Roman numerals
	* 5 - "1" Arabic numerals
	* default value is 0 
*/
	List(int order);

/*!      Constructor whith parameter 0.
*/
        List();
/*!	Destructor	
*/
        ~List();
    /*! Add element to select position.
        \param element Value of this ellement.
	\param i Position in the list.
     */
        void addElementAt(const std::string & element,unsigned int i);
    /*! Add element to end of the list.
        \param element Value of this ellement.
     */
        void addLastElement(const std::string & element);
    /*! Add element to begin of the list.
        \param element Value of this ellement.
     */
        void addFirstElement(const std::string & element);
    /*! Delete element from select position.
        \param i Position in the list.
     */
        void deleteElementAt(unsigned int i);
    /*! Delete element from end of the list.
        \param element Value of this ellement.
     */
        void deleteLastElement();
    /*! Delete element from begin of the list.
        \param element Value of this ellement.
     */
        void deleteFirstElement();
    /*! Write List object to stream in html format. This method are used automatically Output class.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
};


}//end wmi namespace
}//end daq namespace

#endif

