#ifndef _WMI_SERVER_
#define _WMI_SERVER_
#include <ipc/object.h>
#include <wmi/wmi.hh>
#include <ipc/alarm.h>
#include <ipc/signal.h>
#include <vector>

#include <unistd.h>
#include <dlfcn.h>
#include <pthread.h>
#include "wmi/htmloutputstream.h"
#include "wmi/pluginbase.h"

typedef ::IPCNamedObject<POA_wmi::server,::ipc::single_thread> WMIIPCNamedObject;

namespace daq{

namespace wmi{


class WMIServer: public WMIIPCNamedObject
{
private:
    std::vector< ::wmi::StorePluginInfo > m_infoPlugins;
    std::vector<pthread_t> m_tid;
    std::string m_outputDir;
    std::string m_httpServer;
    std::string m_httpRootDir;
    std::string m_outputImpl;
    std::string m_httpServerRoot;
    bool	m_local;
    void copyScriptToServer(std::string path);
    
public:
    WMIServer(const IPCPartition & p, const std::string & name);
    ~WMIServer();
    void initialize(const ::wmi::InfoPlugin& plInfos, const ::wmi::ServerParameters& params);
    void copyToHTTPServer( const std::string & pluginName );
    void startPlugins();
    void stopPlugins();
    void shutdown();
    void setPageName(OutputStream* out,std::string name);
    void setPluginName(OutputStream* out,std::string name);
    void setOutputDir(OutputStream* out,std::string name);
    void setScriptDir(OutputStream* out,std::string name);
    void setUseScript(OutputStream* out,bool value);
    std::string getHTTPServerRoot();	
};

}//end wmi namespace
}//end daq namespace

#endif
