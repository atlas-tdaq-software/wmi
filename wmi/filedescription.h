#ifndef WMI_FILE_DESCRIPTION_INCLUDED
#define WMI_FILE_DESCRIPTION_INCLUDED
/*! \file filedescription.h Defines the description of output file.
 * \author Evgeny Aleksandrov
 * \version 1.1
 */

#include <string>
#include <vector>
#include <map>
//#include <fstream>
//#include <wmi/utils.h>
//#include <iostream>
namespace daq{

namespace wmi{
struct HrefFile{
	std::string rel;
	std::string type;
	std::string href;
};
class FileDescription{
friend class OutputStream;
friend class HTMLOutputStream;
private:
	//std::ofstream m_file;
	std::vector < std::string > m_script_file;
	std::vector < HrefFile > m_href_file;
	unsigned int m_max_id;
	std::string m_http_root;
	std::vector < std::string > m_variables;
	unsigned int m_variable_number;
	//std::map<std::string,std::ostream&> m_script_function;
	FileDescription(const std::string &http_root);
public:
	//FileDescription();
	//void addScriptFunction(std::string name,std::ostream& function);
	void addScriptFile(const std::string &path);
	void addHrefFile(const std::string &_rel,const std::string &_type,const std::string &_href);
	std::vector < std::string > getScriptFiles();
        std::vector < HrefFile > getHrefFiles();
	
	std::string getNewId();
	std::string getNewVariable();
	std::string getHTTPRoot();
};


}//end wmi namespace
}//end daq namespace

#endif

