#ifndef _WMI_FRAMESET_INCLUDED
#define _WMI_FRAMESET_INCLUDED
/*! \file frameset.h Defines the FrameSet class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include <utility>
#include "wmi/frame.h"
#include "wmi/utils.h"
#include <vector>


namespace daq{

namespace wmi{

enum UNIT {PIXELS,PERCENT,ALL_FREE};

/*! \brief  Define the frameset object of html. Insert frameset to the html page. Elements of frameset is FrameBase.
 */

class FrameSet:public FrameBase{
private:
        unsigned int m_columns;
        unsigned int m_lines;
        int m_border;
	std::string m_value_column;
	std::string m_value_row;
	bool m_isBorder;
	int m_spacing;
	bool m_canAddColumn;
	std::vector< FrameBase* > m_frames;

	void AddStringValue2ColRow(UNIT unitVal,unsigned int lenghtVal,bool isRow);	
public:
/*!     Create frameset object with define size of first column and first line.
	 /param unitColumn The unit size of first column.
	 /param lenghtColumn The size of first column. If unitColumn is ALL_FREE, this parameter not used.
         /param unitRow The unit size of first row.
         /param lenghtRow The size of first row. If unitRow is ALL_FREE, this parameter not used.
*/
        FrameSet(unsigned int lenghtColumn,UNIT unitColumn,unsigned int lenghtRow,UNIT unitRow);
    /*! Destructor.
    */
        ~FrameSet();
    /*! Write frameset object to stream in html format. This method are used automatically output.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
/*!     Add element to table.
        \param i          Number of column. Begin at 1.
        \param j          Number of row. Begin at 1.
        \param Obj        Pointer of element.
*/
        bool addElement(unsigned int i,unsigned int j,FrameBase* Obj);
/*!     Add one row to end of frameset. 
         /param unitRow The unit size of new row.
         /param lenghtRow The size of new row. If unitRow is ALL_FREE, this parameter not used.
*/
        void addRow(unsigned int lenghtRow,UNIT unitRow);
/*!     Add one column to end of frameset. Can added column only befor first add row.
         /param unitColumn The unit size of new column.
         /param lenghtColumn The size of new column. If unitColumn is ALL_FREE, this parameter not used.
*/
        bool addColumn(unsigned int lenghtColumn,UNIT unitColumn);
/*!     Set border of frameset in pixels
	 /param bordersize The size of border.
*/ 
	void setBorder(unsigned int bordersize); 
    /*! Set frame border.
        \param border If true border exists.
    */
        void setFrameBorder(bool border);
/*!     Set spacing (without border) of frameset in pixels
         /param spacingsize The size of spacing.
*/
        void setFrameSpacing(unsigned int spacingsize);

};

}//end wmi namespace
}//end daq namespace

#endif

