#ifndef _WMI_PICTURE_INCLUDED
#define _WMI_PICTURE_INCLUDED
/*! \file picture.h Defines the Picture class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Define the img object of html. Add picture to the page. Picture can be one of following format:
	* JPEG,GIF,PNG.
 */
class Picture: public BaseHTMLObject{
        long m_length;
        std::string m_link;
        unsigned int m_height;
        unsigned int m_width;
	bool m_needCopy;
        unsigned char* m_picture_data;
public:
/*!     Create picture object.The size of picture on html page define Width=200,Height=200.
	\param ln Path to picture file. Can be absolute or not absolute.
*/
        Picture(const std::string & ln);
/*!     Create picture object.
        \param ln Path to picture file. Can be absolute or not absolute.
	\param w Width picture in pixels on html page.
	\param h Height picture in pixels on html page. 
*/
        Picture(const std::string & ln,unsigned int w,unsigned int h);
/*!     Create picture object from array data. The size on html page of picture define Width=200,Height=200.
        \param size Lenngth of array data.
        \param data[] Array data.
        \param ln Name of picture file.
*/
	Picture(long size,unsigned char data[], const std::string & ln);
/*!     Destroy picture object.
*/
        ~Picture();
/*!     Set the parameter of copy.
        \param needCopy If true the source file needs copy to working directory of plug-in, otherwise this file already in this directory. Default value is false.
*/
	void setNeedCopy(bool needCopy);
/*!     Set the size of picture on html page.  
        \param w Width picture in pixels on html page.
        \param h Height picture in pixels on html page.
*/
	void setSize(unsigned int w,unsigned int h);
    /*! Write picture object to stream in html format.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
};


}//end wmi namespace
}//end daq namespace

#endif

