#ifndef _WMI_TIMEGRAPH_INCLUDED
#define _WMI_TIMEGRAPH_INCLUDED
/*! \file .h Defines the TimeGraph class.
 * \author Evgeny Aleksandrov
 * \version 1.1
 */

#include <string>
#include <iostream>
#include <utility>
#include "wmi/basehtmlobject.h"
#include "wmi/utils.h"
namespace daq{

namespace wmi{

/*! \brief  This object define a graph. Axis X of this graph is a Date. This graph can zoom axis x. This object used java scripts. 
        
 */
class TimeGraph: public BaseHTMLObject{
	std::vector<std::pair<unsigned long,float> > m_poits;
	int m_style;
	int m_color;
	std::string m_height;
	std::string m_width;
	std::string m_name;
	std::string m_yLabel;
	std::pair<float,float> m_min_max_y;
public:
/*!     Create TimeGraph object.
	 \param name Name of this graph.
	 \param yLabel Laybel of axis Y. Now not used but add to API becouse in future may be used.
	 \param poits Points of graph. First value is the number of seconds elapsed since midnight (00:00:00), January 1, 1970.
	 \param min_max_y A pair of min and max value of axis Y. This parameter not required.     
*/
	TimeGraph(const std::string & name,const std::string & yLabel,std::vector<std::pair<unsigned long,float> > poits,unsigned int width,unsigned int height,std::pair<float,float> min_max_y = std::make_pair (-1,-1) );
/*!     Create TimeGraph object.
         \param name Name of this graph.
	 \param yLabel Laybel of axis Y. Now not used but add to API becouse in future may be used.
         \param poits Points of graph. First value is the number of seconds elapsed since midnight (00:00:00), January 1, 1970.
*/
        TimeGraph(const std::string & name,const std::string & yLabel,std::vector<std::pair<unsigned long,float> > poits);
/*!     Create TimeGraph object.
         \param name Name of this graph.
	 \param yLabel Laybel of axis Y. Now not used but add to API becouse in future may be used.
         \param poits Points of graph. First value is the number of seconds elapsed since midnight (00:00:00), January 1, 1970.
	 \param height The height of applet
	 \param isHPercent Is the height of applet in percent.
         \param width The width of applet
         \param isHPercent Is the width of applet in percent.
*/
        TimeGraph(const std::string & name,const std::string & yLabel,std::vector<std::pair<unsigned long,float> > poits,unsigned int width,bool isWPercent,unsigned int height,bool isHPercent);

    /*! Write TimeGraph object to stream in html format.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
};


}//end wmi namespace
}//end daq namespace

#endif

