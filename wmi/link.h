#ifndef _WMI_LINK_INCLUDED
#define _WMI_LINK_INCLUDED
/*! \file link.h Defines the link class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Define the link object of html. Create the link on html page.
 */

class Link: public BaseHTMLObject{
private:
        std::string m_href;
	std::string m_text;
public:
    /*! Constructor.
        \param href Link to file. Invisible on html page.
	\param text Name of this link. This name visible on the html page.
	\param need_replace Need replace special symbol (like #,< and so on). If false href used without replace.
     */
        Link(const std::string & href,const std::string & text,bool need_replace=true);
    /*! Destructor.
    */
        ~Link();
    /*! Write Link object to stream in html format. This method are used automatically Output class.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
};


}//end wmi namespace
}//end daq namespace

#endif

