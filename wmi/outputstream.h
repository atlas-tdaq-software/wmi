#ifndef WMI_OUTPUT_STREAM_INCLUDED
#define WMI_OUTPUT_STREAM_INCLUDED
/*! \file outputstream.h Defines the OutputStreame abstract class for all output class of wmi.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include "wmi/basehtmlobject.h"
#include "wmi/frame.h"
#include "wmi/frameset.h"

namespace daq{

namespace wmi{
/*! \brief  Base abstract class for all output class of wmi. All output class of wmi  must implementation all abstract function of this class.
    \sa
 */

class OutputStream{
friend class WMIServer;
protected:
        std::string m_pluginName;
	std::string m_partitionName;
	std::string m_outputDir;
	std::string m_scriptDir;
	bool m_isOpened;
	std::string m_pageName;
	unsigned int m_period;
	std::string m_bgColor;
	int m_currentIndex;
	int m_maxIndex;
	bool m_useScript;
	std::map<std::string,int> m_index_map;
	std::map<int,void*> m_file_map;
	std::map<int,void*> m_file_description_map;

	void deleteIndexFromMap(int ind);
	int getNewIndex();
	int getFileDescriptor(std::string fname);
	void setPluginName(std::string st);
	void setPageName(std::string name);
	void setOutputDir(std::string st);
	void setScriptDir(std::string st);
	void setUseScript(bool value);
	std::string getFileName(int descriptor);
public:

    /*! Virtual function for open file with name index.html.
        \return File descriptor.
     */	
	virtual int open() = 0;
    /*! Virtual function for open file.
	\param fileName The name of file.
	\return File descriptor.
     */
        virtual int open(const std::string & fileName) = 0;
    /*! Virtual function for open file.
        \param fileName The name of file.
	\param dirName The name of directory where this file will be created.
        \return File descriptor.
     */
	virtual int open(const std::string & fileName,const std::string &dirName) = 0;
    /*! Virtual function for close file.
        \param ind The file descriptor.
     */
	virtual void close(int ind) = 0;
    /*! Virtual function for write header of file to file.
        \param fileDescriptor The file descriptor where this object need write.
     */
	virtual void writeWithScript(int fileDescriptor) = 0;
    /*! Virtual function for write object to file.
        \param tb WMI html object.
        \param fileDescriptor The file descriptor where this object need write.
     */
	virtual void write(BaseHTMLObject& tb,int fileDescriptor) = 0;
    /*! Virtual function for write frame page to file.
        \param fileName The name of file.
        \param frameset The FrameSet object of file which need to create.
        \param dirName The name of directory where this file will be created.
     */
        virtual bool writeFramePage(const std::string &fileName,const FrameSet& frameset,const std::string &dirName="") = 0;
    /*! Virtual function for close all files. Recommend use this methods when plug-in finish the work.
     */
	virtual void closeAll() = 0;
    /*! Get pointer of FileDescription using file descriptor.
	\param ind File descriptor.
	\return Pointer of FileDescription.If description not found return NULL.	
     */
        FileDescription* getFileDescription(int ind);
    /*! Create and write info page.
        \param fileName The name of file.
        \param message Message which will write in HTML format to file.
     */
	void createInfoPage(std::string fileName,std::string message);
    /*! Get plug-in name.
	\return Plug-in name.
     */
	std::string getPluginName();
    /*! Get path to outpur directory.
        \return outpur directory.
     */
	std::string getOutputDir();
    /*! Set current file descriptor.
        \param ind File descriptor of file.
     */
	void setCurrentIndex(int ind);
    /*! Close file with current file descriptor.
     */
	void close();
    /*! Write object to file with current file descriptor.
        \param tb WMI html object.
     */
	void write(BaseHTMLObject& tb); 
    /*! Destructor.
     */
        virtual ~OutputStream(){};
};


}//end wmi namespace
}//end daq namespace

#endif

