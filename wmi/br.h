#ifndef _WMI_BR_INCLUDED
#define _WMI_BR_INCLUDED
/*! \file br.h Defines the br class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */
#include <iostream>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Define the br object of html.
 */

class BR: public BaseHTMLObject{
public:
    /*! Empty constructor.
    */
        BR();
    /*! Destructor.
    */
        ~BR();
    /*! Write br object to stream in html format.
	\param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
	
};


}//end wmi namespace
}//end daq namespace

#endif

