#ifndef _WMI_TOOLTIP_INCLUDED
#define _WMI_TOOLTIP_INCLUDED
/*! \file hr.h Defines the hr class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <iostream>
#include <string>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

/*! \brief  Implements tooltip HTML object.
 */
class Tooltip: public BaseHTMLObject{
public:
    /*! Empty constructor.
    */
    Tooltip(const std::string & text);
    /*! Destructor.
    */

    ~Tooltip();

    /*! Writes tooltip object to stream in html format.
        \param str The output stream.
     */
    std::ostream& write(std::ostream& str)  const;

private:
    std::string m_text;
};


}//end wmi namespace
}//end daq namespace

#endif

