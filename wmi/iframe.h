#ifndef _WMI_IFRAME_INCLUDED
#define _WMI_IFRAME_INCLUDED
/*! \file iframe.h Defines the hr class.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */


#include <iostream>
#include "wmi/basehtmlobject.h"
#include "wmi/utils.h"

namespace daq{

namespace wmi{

/*! \brief  Define scrolling enumeration.
 */
//enum Scrolling {YES,NO,AUTO,NODEFINE};
/*! \brief  Define aling enumeration.
 */
enum Aling {ABSMIDDLE,BASELINE,BOTTOM,LEFT,MIDDLE,RIGHT,TEXTTOP,TOP,NODEFINE_ALING}; 

/*! \brief  Define the iframe object of html. Inserts into the html usual page a another independent document 
 * (unlike frame wich can added only special HTML page.  
 */
class IFrame: public BaseHTMLObject{
	std::string m_src;
	std::string m_name;
	std::string m_height;
	std::string m_width;
	Scrolling m_scroll;
	bool m_border;
	Aling m_aling;
	unsigned int m_hspace;
	unsigned int m_vspace;
public:
    /*! Constructor.
	\param src Path to HTML page.
        \param name Name of iframe. May be need for link from other documents using attribut taget.
	\param width The width of frame (may be pixels or percent).
	\param isWPersent Is width in percent
	\param height The height of frame (may be pixels or percent).
	\param isHPersent Is height in percent	
    */
        IFrame(const std::string & src,const std::string & name,unsigned int width,bool isWPersent,unsigned int height,bool isHPersent);
    /*! Destructor.
    */
        ~IFrame();
    /*! Write iframe object to stream in html format. This method are used automatically output.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;
    /*! Set scrolling of frame
        \param scroll Enum of Scrolling.
	\sa Scrolling
    */
        void setScroll(Scrolling scroll);
    /*! Set frame border.
        \param border If true border exists.
    */
        void setFrameBorder(bool border);

    /*! Set aling of iframe
        \param aling Enum of Aling.
	\sa Aling
    */
        void setAling(Aling aling);
    /*! Set invisible horizontal indent.
        \param hspace Indent in pixels.
    */
	void setIndentHorizontal(unsigned int hspace);
    /*! Set invisible vertical indent.
        \param vspace Indent in pixels.
    */
	void setIndentVertical(unsigned int vspace);
};


}//end wmi namespace
}//end daq namespace

#endif

