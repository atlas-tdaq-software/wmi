#ifndef _FONT_INCLUDED
#define _FONT_INCLUDED
/*! \file font.h Defines the Font and Color.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>

namespace daq{

namespace wmi{

/*! \brief  Define the color in wmi.
 */

class Color{
        std::string m_color_hex; //in hex format
public:
    /*! Empty constructor define black color.
     */	
        Color();
    /*! Special constructor. Automatically used (with parameter is false) for background color if html class can not use background color. 
	\param isExist If value false then color string not define (color="") and color not used otherwise color is black. 
     */
	Color(bool isExist);
    /*! Constructor define color in RGB.
	\param r Red part of color.
        \param g Green part of color.
        \param b Blue part of color.
     */
        Color(unsigned int r,unsigned int g,unsigned int b);
    /*! Destructor of color.
     */
        ~Color(){};
    /*! Write color in form which define in the html format.Use for write html page.
     */
        std::string write();
    /*! Get color in hex format.
     */
        std::string getColor() const;

        Color& operator= ( const Color& ft){
                if ( this != &ft ){
                        m_color_hex = ft.m_color_hex;
                }
                return *this;
        }
        bool operator== (const Color& ft){
                if(m_color_hex.compare(ft.m_color_hex)== 0)
                        return true;
                else
                        return false;
        }
};

/*! \brief  Define the font in wmi.
 */

class Font{
//        friend std::string Text::write(std::ostream& str) const;
//        friend std::string Text::writeEnd() const;

        unsigned int m_size; // size iz value from 1..7
        Color m_color;
	bool m_used;
//	std::string writeStart() const;
//	std::string writeEnd() const;
public:
    /*! Empty constructor define black color and size 3.
     */
        Font();
    /*! Constructor define the font
        \param s Size of the font.
     */
	Font(unsigned int s);
    /*! Constructor define the font
	\param s Size of the font.
	\param cl Color of the font.
     */
        Font(unsigned int s,const Color& cl);
    /*! Destructor of font.
     */
        ~Font();
    /*! Set size of font.
        \param i Size of the font.
     */
        void setSize(unsigned int i);
    /*! Set color of font.
        \param cl Color of the font.
     */
        void setColor(const Color& cl);
    /*! Write font in form which define in the html format. Dont write close tag of font.
     */
        std::string writeStart() const;
    /*! Write close tag of font in form which define in the html format. 
     */
        std::string writeEnd() const;
    
	Font& operator= ( const Font& ft){
		if ( this != &ft ){
			m_color = ft.m_color;
			m_size = ft.m_size;
			m_used = ft.m_used;
		}
		return *this;
	}


};

}//end wmi namespace
}//end daq namespace

#endif


