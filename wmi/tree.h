#ifndef _WMI_TREE_INCLUDED
#define _WMI_TREE_INCLUDED
/*! \file tree.h Defines the Tree and Node classes.
 * \author Evgeny Aleksandrov
 * \version 1.0
 */

#include <string>
#include <iostream>
#include "wmi/basehtmlobject.h"

namespace daq{

namespace wmi{

class Tree;
/*! \brief  Define Node (tree) in c++.
*/
class Node{
friend class Tree;
private:
	std::string m_value;
	bool m_visible;
	Node* m_previous;
	std::vector<Node*> m_children;
	void setParent(Node*);
	Node* getChildConst(unsigned int num) const;
	//std::ostream& writeNode(std::ostream& str);
public:
/*!     Create node object.
        \param name Define value (name) of this node.
*/
	Node(const std::string& name,bool isVisible=true);
/*!     Destroy node object.
*/
	~Node();
    /*! Get the number of children for this node.
        \return Number of children.
     */
	unsigned int getChildrenCount();
    /*! Append new children to this node.
        \param node The pointer of new node.
     */
	void appendChildren(Node* node);
    /*! Append new children to this node.
        \param node The value (name) of new node.
	\return The pointer of new child node.
     */
	Node* appendChildren(const std::string& name,bool isVisible=true);
    /*! Get parent node.
        \return The pointer of parent node. If this node has not parent return NULL.
     */
	Node* getParent();
    /*! Get child node.
	\param num The number of child for this node. Number bigin at 0.
        \return The pointer of child node. If this node has not child with this number return NULL.
     */
	Node* getChild(unsigned int num);
};

/*! \brief  Define Tree for WMI.
*/
class Tree: public BaseHTMLObject{
private:
	Node* m_node;
	unsigned int m_width;
	unsigned int m_height;
	std::string m_name;
	void writeNode(std::ostream& str,const Node* node,std::string prev_id) const;
public:
/*!     Create tree object.
        \param node Pointer of node.
	\param width The width of tree.
	\param height The height of tree.
*/
	Tree(const std::string & name,Node* node,unsigned int width,unsigned int height);
/*!     Destroy tree object.
*/
	~Tree();
    /*! Write tree object to stream in html format.
        \param str The stream for write object.
     */
        std::ostream& write(std::ostream& str)  const;

};

}//end wmi namespace
}//end daq namespace

#endif

