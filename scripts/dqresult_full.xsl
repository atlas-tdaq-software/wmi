<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="UTF-8"/>
<xsl:template match="/objects/obj">
<table bgcolor="#bac8ec" width="100%" cellspacing="1" cellpadding="1">
    <TR><TD>
    <table bgcolor="#bac8ec">
	<xsl:variable name="statuscolor">
	    <xsl:choose>
		<xsl:when test="attr/v=-1"><xsl:value-of select="'#000000'"/></xsl:when>
		<xsl:when test="attr/v=0" ><xsl:value-of select="'#AAAAAA'"/></xsl:when>
		<xsl:when test="attr/v=1" ><xsl:value-of select="'#FF0000'"/></xsl:when>
		<xsl:when test="attr/v=2" ><xsl:value-of select="'#FFDC00'"/></xsl:when>
		<xsl:when test="attr/v=3" ><xsl:value-of select="'#00FF00'"/></xsl:when>
		<xsl:otherwise>		   <xsl:value-of select="'#BAC8EC'"/></xsl:otherwise>
	    </xsl:choose>
	</xsl:variable>
	
	<TR bgcolor="#BAC8EC">
	   <TD><font size="3" color="#000000"><xsl:value-of select="substring-after(@name, '.')" /></font></TD>  	
	   <TD width="20"> </TD>
	   <TD bgcolor='{$statuscolor}' width="20"></TD>
	   <TD width="20"> </TD>
	   <TD><font size="1" color="#000000"><xsl:value-of select="substring-before(@time, '.')" /></font></TD>
	</TR>
    </table>
    </TD></TR>
    <TR bgcolor="#FFFFFF">	
	<TD><font size="2" color="#606060">
	    <xsl:for-each select="attr">
		<xsl:if test="@name='tags'">
		    <xsl:for-each select="v">
			<xsl:value-of select="obj/attr[1]/v"/>
			<xsl:text>=</xsl:text>
			<xsl:value-of select="obj/attr[2]/v" />
			<xsl:text>; </xsl:text>
		    </xsl:for-each>
		</xsl:if>
	    </xsl:for-each>
	</font></TD>  	
    </TR>
</table>
</xsl:template>
</xsl:stylesheet>
