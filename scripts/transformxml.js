function getLibraryPath()
{
    var libraryPath="./";
    var scripts=document.getElementsByTagName("script");
    for(var i=0;i<scripts.length;i++)
    {
	if( scripts[i].getAttribute("src").indexOf("transformxml.js") > 0
		&& scripts[i].getAttribute("data-path") )
	{
	    libraryPath=scripts[i].getAttribute("data-path");
	    break;
	}
    }
    if(libraryPath.charAt(libraryPath.length-1)!="/")
    {
    	libraryPath+="/";
    }
    return libraryPath;
}

function loadXMLDoc(fname)
{
    var xmlDoc;
    // code for IE
    if (window.ActiveXObject)
    {
	xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
    }
    // code for Mozilla, Firefox, Opera, etc.
    else if (document.implementation
	  && document.implementation.createDocument)
    {
	xmlDoc=document.implementation.createDocument("","",null);
    }
    else
    {
	alert('Your browser cannot handle this script');
    }
    
    try {
	xmlDoc.async=false;
	xmlDoc.load(fname);
	return(xmlDoc);
    }
    catch(e)
    {
	try //Google Chrome
	{
	    var xmlhttp = new window.XMLHttpRequest();
	    xmlhttp.open("GET",fname,false);
	    xmlhttp.send(null);
	    xmlDoc = xmlhttp.responseXML;
	    return(xmlDoc);
	}
	catch(e)
	{
	    error=e.message;
	}
    }
}

function transformXML( sourceXML, config )
{
    var sourceXSL = getLibraryPath() + config;
    
    // Load the XML
    var xml = loadXMLDoc(sourceXML);
    
    // Load the XSL
    var xsl = loadXMLDoc(sourceXSL);
    
    // Test Browser's Support
    if(window.ActiveXObject)
    {   // Internet Explorer
        // Transform and display
        document.write( xml.transformNode(xsl) );
    }
    else if (document.implementation
	  && document.implementation.createDocument)
    {
        // Transform and display
        var xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        var xmlDoc = xsltProcessor.transformToDocument(xml);
        var xmls = new XMLSerializer();
        document.write( xmls.serializeToString(xmlDoc) );
    }
    else
    {
        document.write( 'sorry, your browser does not support this...' );
    }
}
