#include "wmi/xml/tinyxml.h"
#include "wmi/server/wmiserver.h"

#include <ipc/core.h>
#include <ipc/alarm.h>
#include <ipc/object.h>
#include <ipc/signal.h>

#include <cmdl/cmdargs.h>

#include <signal.h>

using namespace daq::wmi;

int main(int argc, char** argv)
{
    // Declare arguments
    CmdArgStr config_file('c', "config", "config-file", "configuration file.",
	CmdArg::isREQ);
    CmdArgStr plugin_name('n', "plugin", "plugin-name", "run this plugin only.");
    CmdArgStr server_name('s', "server", "server-name",
	"register in IPC with that name.");
    CmdArgStr partition_name('p', "partition", "partition-name", "partition to work in.");

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &config_file, &plugin_name, &partition_name, &server_name, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    // Parse arguments
    cmd.parse(arg_iter);

    std::string pluginName = (plugin_name.isNULL() ? "" : plugin_name);
    std::string serverName = (server_name.isNULL() ? "" : server_name);

    TiXmlDocument doc((const char*) config_file);
    bool loadOkay = doc.LoadFile();
    if (!loadOkay)
    {
	std::ostringstream txt;
	txt << "Could not load config file \"" << (const char*) config_file << "\"";
	ers::error(ers::Message(ERS_HERE, txt.str()));
	return -1;
    }
    TiXmlNode* node = 0;
    TiXmlElement* Element = 0;
    TiXmlElement* plNode = 0;
    node = doc.FirstChild("setup");
    if (!node)
    {
	ers::error(ers::Message(ERS_HERE, "No root element in configuration file"));
	return -1;
    }
    Element = node->FirstChildElement("HTTPServer");
    std::string httpServer;
    if (Element)
    {
	httpServer = Element->Attribute("name");
    }
    else
    {
	ers::error(ers::Message(ERS_HERE, "Invalid HTTPServer in config file"));
	return -1;
    }

    Element = node->FirstChildElement("HTTPRootDir");
    std::string httpRootDir;
    if (Element)
    {
	httpRootDir = Element->Attribute("path");
	if (httpRootDir.length() > 0)
	{
	    if (!(httpRootDir.find_last_of("/") == (httpRootDir.length() - 1)
		|| httpRootDir.find_last_of("\\") == (httpRootDir.length() - 1)))
		httpRootDir += "/";
	}
    }
    else
    {
	ers::error(ers::Message(ERS_HERE, "Invalid HTTPRootDir in config file"));
	return -1;
    }

    Element = node->FirstChildElement("OutputDir");
    std::string outputDir;
    if (Element)
    {
	outputDir = Element->Attribute("path");
	if (outputDir.length() > 0)
	{
	    if (!(outputDir.find_last_of("/") == (outputDir.length() - 1)
		|| outputDir.find_last_of("\\") == (outputDir.length() - 1)))
		outputDir += "/";
	}
    }
    else
    {
	ers::error(ers::Message(ERS_HERE, "Invalid outputDir in config file"));
	return -1;
    }
    Element = node->FirstChildElement("WMIOutputImpl");
    std::string wmiOutputName;
    if (Element)
    {
	wmiOutputName = Element->Attribute("name");
    }
    else
    {
	ers::error(ers::Message(ERS_HERE, "Invalid WMIOutputImpl in config file"));
	return -1;
    }
    Element = node->FirstChildElement("HTTPServerRoot");
    std::string wmiHTTPServerRoot = "";
    if (Element)
    {
	wmiHTTPServerRoot = Element->Attribute("path");
    }
    else
    {
	ers::warning(ers::Message(ERS_HERE, "Invalid HTTPServerRoot in config file"));
    }
    Element = node->FirstChildElement("ScriptDir");
    std::string wmiScriptDir = "";
    if (Element)
    {
	wmiScriptDir = Element->Attribute("path");
    }
    else
    {
	ers::warning(ers::Message(ERS_HERE, "Invalid ScriptDir in config file"));
    }
    node = node->FirstChild("plugins");
    if (!node)
    {
	ers::error(ers::Message(ERS_HERE, "Invalid plugins in config file"));
	return -1;
    }
    plNode = node->FirstChildElement("plugin");
    if (!plNode)
    {
	ers::error(ers::Message(ERS_HERE, "Error plugin not found"));
	return -1;
    }

    std::vector < wmi::StorePluginInfo > storePluginInfo;
    wmi::ServerParameters serverParameters;
    serverParameters.outputDir = outputDir.c_str();
    serverParameters.httpServer = httpServer.c_str();
    serverParameters.httpRootDir = httpRootDir.c_str();
    serverParameters.outputImpl = wmiOutputName.c_str();
    serverParameters.httpServerRoot = wmiHTTPServerRoot.c_str();
    serverParameters.scriptDir = wmiScriptDir.c_str();
    while (plNode)
    {
	try
	{
	    wmi::StorePluginInfo pluginInfo;
	    pluginInfo.name = plNode->Attribute("name");
	    pluginInfo.fullPath = plNode->Attribute("fullPath");
	    pluginInfo.useScript = true;
	    try
	    {
		std::string use_script = plNode->Attribute("useScript");
		if (use_script == "no" || use_script == "NO" || use_script == "No")
		    pluginInfo.useScript = false;
	    }
	    catch (...)
	    {
	    }

	    pluginInfo.outputDir = outputDir.c_str();
	    pluginInfo.outputImpl = wmiOutputName.c_str();
	    pluginInfo.period = 0;
	    double per;
	    if (plNode->QueryDoubleAttribute("period", &per) == TIXML_SUCCESS)
	    {
		pluginInfo.period = per;
	    }
	    Element = plNode->FirstChildElement("parameters");
	    std::vector < wmi::Parameter > storeParameters;
	    while (Element)
	    {
		wmi::Parameter parameter;
		parameter.name = Element->Attribute("name");
		parameter.value = Element->Attribute("value");
		storeParameters.push_back(parameter);

		Element = Element->NextSiblingElement("parameters");
	    }
	    pluginInfo.params.length(storeParameters.size());
	    for (unsigned int i = 0; i < storeParameters.size(); i++)
	    {
		pluginInfo.params[i] = storeParameters[i];
	    }
	    storePluginInfo.push_back(pluginInfo);
	    plNode = plNode->NextSiblingElement("plugin");
	}
	catch (...)
	{
	    ers::fatal(ers::Message(ERS_HERE, "Error in config file"));
	    return -1;
	}
    }

    IPCCore::init(argc, argv);

    wmi::InfoPlugin allPluginInfo;
    for (unsigned int i = 0; i < storePluginInfo.size(); i++)
    {
	if (pluginName.empty() || pluginName == (const char *) storePluginInfo[i].name)
	{
	    size_t l = allPluginInfo.length();
	    allPluginInfo.length(l + 1);
	    allPluginInfo[l] = storePluginInfo[i];
	}
    }

    if (!allPluginInfo.length())
    {
	ers::fatal(ers::Message(ERS_HERE, "No suittable plugins found in configuration"));
	return -1;
    }

    IPCPartition partition(partition_name);

    if (serverName.empty())
    {
	serverName = allPluginInfo.length() == 1 ? allPluginInfo[0].name : "server";
    }

    try
    {
	if (partition.isObjectValid < ::wmi::server > (serverName))
	{
	    ers::fatal(ers::Message(ERS_HERE, "WMI server is already running."));
	    return -1;
	}
    }
    catch (daq::ipc::Exception & ex)
    {
	ers::fatal(ex);
	return -1;
    }

    WMIServer * server = new WMIServer(partition, serverName);
    server->initialize(allPluginInfo, serverParameters);
    server->startPlugins();
    ERS_LOG("WMI server started");

    daq::ipc::signal::wait_for();

    ERS_LOG("Stopping WMI server");
    server->withdraw();
    server->stopPlugins();
    server->_destroy(true);

    return 0;
}
