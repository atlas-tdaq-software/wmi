package wmi.plot;

import java.awt.Color;

import javax.swing.JApplet;


public class wmiplot extends JApplet {
	wmipanel pan = null;
	public void init() {
		pan = new wmipanel();
		if(getParameter("points")!= null)
			pan.setPoints(getParameter("points"));
		if(getParameter("title")!= null ){
			pan.plot_name = getParameter("title");
		}
		if((getParameter("bgcolor") != null) && (getParameter("bgcolor").length() >0)){
			//String stBGColor = ;
			try{
				pan.bgAllColor = Color.decode("0x"+getParameter("bgcolor"));
			}
			catch(Exception ex){
				
			}
		}
		pan.install();
		getContentPane().add(pan);
		pan.setVisible(true);
		//start();
	}
	public void start(){
		
	}
	public void destroy() {		
		super.destroy();
	}
}

