package wmi.plot;

//import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
//import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.sql.Time;
import java.text.AttributedString;
import java.text.BreakIterator;
import java.util.Date;

//import javax.swing.JApplet;
//import javax.swing.JButton;
//import javax.swing.JFrame;
import javax.swing.JPanel;
//import javax.swing.JToolTip;

public class wmipanel extends JPanel implements MouseMotionListener{
	//JApplet applet = null;
	Point2D p = null;
	float min_y,max_y;
	long  min_x,max_x;
	private long frame_min_x,frame_max_x;
	private double frame_min_y,frame_max_y;
	long[] x = null;
	float[] y = null;
	int number = 0;
	
	int indentXStart = 50;
	int indentYstart = 30;
	int indentXEnd = 25;
	int indentYEnd = 35;
	int indentYEndMin = 15;
	int numberXValue = 5;
	int numberYValue = 5;
	int maxNumberXValue = 7;
	int maxNumberYValue = 20;
	int sizeLineValue = 5;
	int minXStep = 5;//in sec
	int x_step = 5;
	double y_step = 1;
//	int mixXStep = 5;
	float scalex = 1;
	float scaley = 1;
	FontRenderContext fr = new FontRenderContext(null, true, true);
	Font font = new Font("SansSerif", Font.PLAIN, 10);
	public String plot_name = "noname";
	int title_in_pixels = 0;
	Integer mouse_x_value = null;
	Integer mouse_y_value = null;
	float rm_y_value = 0;
	long rm_x_value = 0;
	boolean isDrawPosition = false;
	int mouseMagneticValue = 3;
	int indentUpToY = 0;
	
	Color lineColor = new Color(60,134,239);
	Color bgAllColor = new Color(244,227,201);
	Color bgGraphColor = new Color(254,248,237);
	
//	JButton btnOptions = new JButton("Options");
	private Font titleFont = font.deriveFont(Font.BOLD,16);

//	JFrame toolTip = new JFrame();
//	Float 
	public wmipanel(){
		setEnabled(true);
//		btnOptions.addActionListener(this);
//		toolTip.setSize(50, 80);
//		btnOptions.setBounds(0, 0, 10, 10);//Size(20, 10);
//		add(btnOptions);
	}
	public void install(){
		addMouseMotionListener(this);
		if(plot_name.length() == 0)
			indentYEnd = indentYEndMin;
	}
	public void addPoint(String point,int i){
		String sx = point.substring(0, point.indexOf(";"));
		String sy = point.substring(point.indexOf(";")+1,point.length());
		long xi = Long.parseLong(sx);
		float yi = Float.parseFloat(sy);
		x[i] = xi;
		y[i] = yi;
		if (i == 0 || xi < min_x)
			min_x = xi;
		if (i == 0 || xi > max_x)
			max_x = xi;
		if (i == 0 || yi < min_y)
			min_y = yi;
		if (i == 0 || yi > max_y)
			max_y = yi;
			
		//p = new Point2D.Double(Integer.parseInt(x),Integer.parseInt(y));
		//Integer.parseInt(x);
	}
	
	public void setPoints(String points){
		//int number = 0;
		try{
			String s_number = points.substring(0, points.indexOf(" "));
			number = Integer.parseInt(s_number);
			//points = points.substring(points.indexOf(" "));
		}
		catch (Exception e) {
			return;
		}
		if (number < 1)
			return;
		x = new long[number];
		y = new float[number];
		for(int i = 0;i<number;i++){
			try{
				points = points.substring(points.indexOf(" ")+1);
				String point;
				if(points.indexOf(" ") != -1)
					point = points.substring(0, points.indexOf(" "));
				else{
					point = points.substring(0, points.length());
					addPoint(point,i);
					number = i+1;
//					if(min_y > 0)
//						min_y = 0;
					if(min_y == max_y){
						if(min_y > 0){
							min_y = 0;
							max_y = (float) (max_y*1.2);
						}
						else{
							max_y = 0;
							min_y = (float) (min_y*1.2);
						}
					}
					else{
						float h = max_y - min_y;
						max_y = max_y + h/10;
					}
					return;
				}
				
				addPoint(point,i);
			}
			catch (Exception e) {
				number = i;
				if(min_y == max_y){
					if(min_y > 0){
						min_y = 0;
						max_y = (float) (max_y*1.2);
					}
					else{
						max_y = 0;
						min_y = (float) (min_y*1.2);
					}
				}
				else{
					float h = max_y - min_y;
					max_y = max_y + h/10;
				}
				return;
			}
		}
		if(min_y == max_y){
			if(min_y > 0){
				min_y = 0;
				max_y = (float) (max_y*1.2);
			}
			else{
				max_y = 0;
				min_y = (float) (min_y*1.2);
			}
		}
		else{
			float h = max_y - min_y;
			max_y = max_y + h/10;
		}
//		if(min_y > 0)
//			min_y = 0;
	}
	
//	public void repaint() {
//		super.repaint();
//	}
//	private int getRealY(int y){
//		return getHeight()-10 - y;
//	}
	
	private String getTime(long time){
		Time t = new Time(time);
		return t.toString();
	}
	private void setNumberYValue(FontRenderContext fr){
//		setYStep();
		float h = font.getLineMetrics("(", fr).getHeight();
		int real_h = getHeight() - indentYstart - indentYEnd;
		for(int i=maxNumberYValue;i>0;i--){
			if(real_h > 3*h * i){
				numberYValue = i;
				setYStep();
				frame_min_y = min_y - min_y%y_step;
				frame_max_y = max_y + y_step - (float)(((double)max_y)%y_step);
				double height = frame_max_y - frame_min_y;
				scaley = (float) ((getHeight() - indentYstart - indentYEnd) / (height));
//				if(getRoundValue((frame_max_y-frame_min_y)/(numberYValue))*scaley>3*h)
					return;
			}
		}
		numberYValue = 1;
		if(y_step != 0){
			frame_min_y = min_y - min_y%y_step;
			frame_max_y = max_y + y_step - max_y%y_step;
		}
		else{
			frame_min_y = min_y;
			frame_max_y = max_y;
		}
			
	}
	
	private void setNumberXValue(FontRenderContext fr){
		String test = "00/00/000";
		AttributedString as = new AttributedString(test);
		as.addAttribute(TextAttribute.FONT, font, 0, test.length());
		LineBreakMeasurer lbm = new LineBreakMeasurer(as.getIterator(), BreakIterator.getLineInstance(), fr);
		//int j;
		int real_h = getWidth() - indentXStart - indentXEnd;
		for(int i=maxNumberYValue;i>0;i--){
			int w = (int) (real_h/(i*1.));
			if(w<5)
				continue;
			if(lbm.nextOffset(w - 4) != test.length())
				continue;
			numberXValue = i;
			setXStep();
			if(min_x != max_x){
				frame_min_x = min_x - min_x%x_step;
				frame_max_x = max_x + x_step - max_x%x_step;
			}
			else{
				frame_min_x = min_x;
				frame_max_x = max_x;
			}
				
			return;	
		}
		numberXValue = 1;
		setXStep();
		if(min_x != max_x){
			frame_min_x = min_x - min_x%x_step;
			frame_max_x = max_x + x_step - max_x%x_step;
		}
		else{
			frame_min_x = min_x;
			frame_max_x = max_x;
		}
	}

	private void drawXAxisLabel(Graphics g,int x1,int y1,String st){
		float h = font.getLineMetrics("(", fr).getHeight();
		if(st.length()>9)
			st = st.substring(0, 8);
//		font = g.getFont();
//		String st = "testfh";
		AttributedString as = new AttributedString(st);
		as.addAttribute(TextAttribute.FONT, font, 0, st.length());
		LineBreakMeasurer lbm = new LineBreakMeasurer(as.getIterator(), BreakIterator.getLineInstance(), fr);
		int i = 10;
		for(i=10;i<indentXStart;i=i+2){
			if(lbm.nextOffset(i) == st.length())
				break;
		}
		//i = i+2;
		g.drawString(st, x1 + indentXStart-i/2,(int)( getHeight()-indentYstart-y1+(int)h));
	}	
	
	private void drawYAxisLabel(Graphics g,int x1,int y1,String st){
		if(st.length()>7)
			st = st.substring(0, 6);
//		font = g.getFont();
//		String st = "testfh";
		AttributedString as = new AttributedString(st);
		as.addAttribute(TextAttribute.FONT, font, 0, st.length());
		LineBreakMeasurer lbm = new LineBreakMeasurer(as.getIterator(), BreakIterator.getLineInstance(), fr);
		int i = 10;
		for(i=5;i<indentXStart;i=i+1){
			if(lbm.nextOffset(i) == st.length())
				break;
		}
		i = i+2;
		g.drawString(st, x1 + (indentXStart-i),(int)( getHeight()-indentYstart-y1+frame_min_y*scaley));
	}
	
	private int getPower(double value){
		int power = 1;
		String st_z = (new Float(value)).toString();
		if(value<100000 && value>-100000){
			if (value>-0.00001 && value<0.00001){
				int i = st_z.indexOf("E");
				if(i == -1){
					return power;
				}
				String st = st_z.substring(i+2, st_z.length());
				return Integer.parseInt(st);
				//return result;
			}
			else{
				return 1;
			}	
		}
		else{
			int i = st_z.indexOf("E");
			if(i != -1){
				String st = st_z.substring(i+1, st_z.length());
				return Integer.parseInt(st);
			}
			
//			i = st_z.length() - 1;
//			//st_z = st_z.substring(0,2);
//			String res = st_z.charAt(0) + "." + st_z.charAt(1)+"+E"+i;
//			//st_z = st_z +'*10^'+i;
//			return res;	
		}		
		return power;
	}
	
	private String getFloatValue(float z){
		String st_z = (new Float(z)).toString();
		if(st_z.length() < 7){
			return st_z;
		}
//		int pow = getPower(y_step);
		String result = st_z.substring(0, 4);
		if(z<100000 && z>-100000){
			if (z>-0.00001 && z<0.00001){
				int i = st_z.indexOf("E");
				if(i == -1){
					return st_z;
				}
//				if()
//				int value_power = getPower(z);
//				if(getPower(z) == pow+1){
//					result =st_z.substring(0, 1);
//				}
//				else
					result += st_z.substring(i, st_z.length());
					
				return result;
			}
			else{
				if(z>1000)
					st_z = (new Long((long) z).toString());
				return st_z;
			}	
		}
		else{
			int i;
			i = st_z.indexOf("E");
			if(i != -1){
				int value_power = getPower(z);
//				if(getPower(z) == pow+1){
//					result =st_z.substring(0, 1);
//				}
				result += st_z.substring(i, st_z.length());
				return result;
			}
			
			i = st_z.indexOf(".");
			if(i > 0){
				st_z = st_z.substring(0,i);
			}
			i = st_z.length() - 1;
			String res = st_z.charAt(0) + "." + st_z.charAt(1)+"+E"+i;
			return res;	
		}
//		return z;
	}	
	private double getRealYStep(float h){
		double res = y_step;
//		getRoundValue((frame_max_y-frame_min_y)/(numberYValue))*scaley>3*h);
		return res;
	}
	private void setYStep(){
		y_step = 1;
		float min_max = max_y - min_y;
		if(min_max > 1){
			while(true){
				if(min_max > numberYValue *y_step)
					y_step = y_step*10;
				else{
//					if(y_step != 1)
//						y_step = y_step/100;
//					else
						y_step = y_step/10;
					return;
				}
			}
		}
		else{
			while(true){
				if(min_max < numberYValue *y_step)
					y_step = y_step/10;
				else{
					y_step = y_step/10;
					return;
				}
			}			
		}
	}	
	private void setXStep(){
		x_step = minXStep;
		long realMinXStep = minXStep;
		long min_max = (max_x - min_x);
		if(min_max < minXStep){
			numberXValue = 1;
			x_step = 0;
//			step = mixXStep;
			return;
		}
		else if(min_max < numberXValue * minXStep){
			for(int i = numberXValue-1;i>0;i--){
				if(min_max > numberXValue * x_step){
					numberXValue = i+1;
					return;
				}
			}
		}
		while(true){
			if(min_max < numberXValue * x_step){
//				if(step == minXStep){
				return;
//				}
			}
			if(x_step%86400 == 0 && x_step * numberXValue > 864000)
				realMinXStep = 86400;//1 day
			else if(x_step%36000 == 0 && x_step * numberXValue > 360000)
				realMinXStep = 36000;//10 hour
			else if(x_step%18000 == 0 && x_step * numberXValue > 180000)
				realMinXStep = 18000;//5 hour
			else if(x_step%3600 == 0 && x_step * numberXValue > 36000)
				realMinXStep = 3600;//1 hour
			else if(x_step%1800 == 0 && x_step * numberXValue > 18000)
				realMinXStep = 1800;//30 min
			else if(x_step%600 == 0 && x_step * numberXValue > 6000)
				realMinXStep = 600;//10 min
			else if(x_step%300 == 0 && x_step * numberXValue > 3000)
				realMinXStep = 300;//5 min
			else if(x_step%60 == 0 && x_step * numberXValue > 600)
				realMinXStep = 60;//1 min
			else if(x_step%30 == 0 && x_step * numberXValue > 300)
				realMinXStep = 30;//30 sec
			else if(x_step%10 == 0 && x_step * numberXValue > 100)
				realMinXStep = 10;//10 sec
//			if(x_step == 30)
//				realMinXStep = 30;
//			if(x_step == 60)
//				realMinXStep = 60;
//			if(x_step == 600)
//				realMinXStep = 600;
			
			x_step += realMinXStep;
		}
//		return step;
	}
	
	private double getRoundValue(double value){
		double res = 1;
		if(value > 1){
			while(true){
				if(value > res){
					res = res*10;
				}
				else{
					res = res/10;
					break;
				}
			}
		}
		else{
			while(true){
				if(value < res)
					res = res/10;
				else{
					//res = res/10;
					break;
				}
			}			
		}		
		return (value - value%res);
	}
	private void drawAxis(Graphics g,double scalex,double scaley){
		//float sum = (float)((max_x - min_x)/(1.*numberXValue));
//		int sum = setXStep();		
		
		long i = frame_min_x;
		for(int j = 0;j<=numberXValue+1;j++){
			if(j!= 0)
				i += x_step; 
//		for(float i = 0;i<(max_x-min_x+1);i=i+sum){
			if(j == 0)
				continue;
			if(j == numberXValue+1 && numberXValue !=1)
				continue;
//			if(j == numberXValue && numberXValue != 1)
//				continue;
			int xi = (int)((i-frame_min_x)*scalex);
			Color col = g.getColor();
			g.setColor(Color.lightGray);
			drawLine(g,xi,(int)(frame_min_y*scaley + indentUpToY),xi,(int)(frame_max_y*scaley + indentUpToY));
			g.setColor(col);
			drawLine(g, xi, (int)(frame_min_y*scaley + indentUpToY), xi, (int)(frame_min_y*scaley + indentUpToY)+sizeLineValue);
			Date t = new Date((long)((i)*1000));
//			String st = t.toLocaleString();//toGMTString();
//			String time = st.substring(st.indexOf(" "));
			String time="";
//			if(t.getHours() <10)
//				time +="0";
			time += t.getHours();
			time +=":";
			if(t.getMinutes() <10)
				time +="0";
			time += t.getMinutes();
			time +=":";
			if(t.getSeconds() <10)
				time +="0";
			time += t.getSeconds();
			drawXAxisLabel(g,(int)(xi),0,time);	
			float h = font.getLineMetrics("(", fr).getHeight();
//			Date t = new Date((long)((i+min_x)*1000));
//			st = st.substring(0, st.indexOf(" "));
//			String date = st.substring(0,st.length()-4)+st.substring(st.length()-2);
			String date = "";
			if(t.getMonth()+1 <10)
				date+="0";
			date += (t.getMonth()+1) + "/";
			if(t.getDate() <10)
				date+="0";
			date += (t.getDate());
			drawXAxisLabel(g,(int)(xi),(int)-h,date);	
		}
		//int tmp_y_step = 10/numberYValue;
		double real_y_step =getRoundValue((frame_max_y-frame_min_y)/(numberYValue));// +y_step;
//		double yi = (double)frame_min_y;
//		for(int j = 0;j<=numberYValue;j++){
		for(double yi = frame_min_y;yi<=frame_max_y;yi =yi+real_y_step){
//			if(j!= 0)
//				yi += real_y_step; 
			Color col = g.getColor();
			g.setColor(Color.lightGray);
			drawLine(g, 0, (int)(yi*scaley), (int)((frame_max_x-frame_min_x)*scalex), (int)(yi*scaley));
			g.setColor(col);
			drawLine(g, 0, (int)(yi*scaley), sizeLineValue, (int)(yi*scaley));
			drawYAxisLabel(g,0,(int)(yi*scaley),getFloatValue((float)yi));
		}
		

//		float sum = (float)((max_y - min_y)/numberYValue);
//		for(float ii = min_y;ii<=max_y;ii=ii+sum){
//			if(ii<0 && (ii+sum)>0){
//				if(ii == min_y){
//					drawLine(g, 0, (int)(ii*scaley), sizeLineValue, (int)(ii*scaley));
//					drawYAxisLabel(g,0,(int)(ii*scaley),(new Float(ii)).toString());	
//				}				
//				ii = 0;
//				drawYAxisLabel(g,0,(int)(0*scaley),(new Float(0)).toString());
//				continue;
//			}
//			if(ii+sum>max_y){
//				ii= max_y;
//			}
//			//int yi = (int)((max_y - min_y)*scalex*i/numberYValue);
//			Color col = g.getColor();
//			g.setColor(Color.lightGray);
//			drawLine(g, 0, (int)(ii*scaley), (int)((frame_max_x-frame_min_x)*scalex), (int)(ii*scaley));
//			g.setColor(col);
//			drawLine(g, 0, (int)(ii*scaley), sizeLineValue, (int)(ii*scaley));
////			Float fl = new Float(i);
////			String val = fl.toString();
//			drawYAxisLabel(g,0,(int)(ii*scaley),getFloatValue(ii));
//		}
		drawLine(g, 0,(int)(frame_min_y*scaley + indentUpToY), (int)((frame_max_x - frame_min_x) * scalex), (int)(frame_min_y*scaley + indentUpToY));
		drawLine(g, 0,(int)(frame_max_y * scaley+ indentUpToY), 0, (int)(frame_min_y * scaley));
	}
	private void drawLine(Graphics g,int x1,int y1,int x2,int y2){
		drawLine(g, x1, y1, x2, y2, false);
	}
	private void drawLine(Graphics g,int x1,int y1,int x2,int y2,boolean useStroke){
		Stroke str = null;
		if (useStroke && g instanceof Graphics2D){
			str = ((Graphics2D)g).getStroke();
//			((Graphics2D)g).setStroke(new BasicStroke(2,BasicStroke.CAP_SQUARE,BasicStroke.JOIN_ROUND));
			((Graphics2D)g).setRenderingHint (RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);                         
			((Graphics2D)g).setStroke( new java.awt.BasicStroke( 2.0f ) );
		}
//		if(frame_min_y != frame_max_y)
			g.drawLine(x1+indentXStart, (int)(getHeight()-indentYstart-y1+frame_min_y*scaley), x2+indentXStart, (int)(getHeight()-indentYstart-y2+frame_min_y*scaley));//+frame_min_y*scaley));
//		else
//			g.drawLine(x1+indentXStart, (int)(getHeight()-indentYstart-y1), x2+indentXStart, (int)(getHeight()-indentYstart-y2));//+frame_min_y*scaley));
				
		if(str != null)
			((Graphics2D)g).setStroke(str);
	}
//	private void fillRect(Graphics g,int x1,int y1,int x2,int y2){
//		int width = x2- x1;
//		int height = y1-y2;
//		g.fillRect(x1+indentXStart, (int)(getHeight()-indentYstart-y1+min_y*scaley), x2+indentXStart, (int)(getHeight()-indentYstart-y2+min_y*scaley));
//	}
	private void drawTitle(Graphics g,int x1,int y1){
		if(plot_name.length() == 0)
			return;
		float h = titleFont.getLineMetrics("(", fr).getHeight();
		AttributedString as = new AttributedString(plot_name);
		as.addAttribute(TextAttribute.FONT, titleFont, 0, plot_name.length());
		LineBreakMeasurer lbm = new LineBreakMeasurer(as.getIterator(), BreakIterator.getLineInstance(), fr);
		int i = 10;
		int j = 0;
		for(i=10;i<getWidth()-20;i=i+5){
			if(lbm.nextOffset(i) == plot_name.length()){
				j=i;
				break;
			}
		}
		title_in_pixels = j+5;
//		if (j<0)
//			add(btnOptions);
		
		g.setFont(titleFont);
//		g.drawString(plot_name, x1, (int)(y1 + h));
		g.drawString(plot_name, (getWidth() - title_in_pixels)/2, (int)(y1 + h));
		g.setFont(font);
		
	}
	
	private void drawMouseValues(Graphics g){
		float h = titleFont.getLineMetrics("(", fr).getHeight();
		String x_value = "X: ";
		String y_value = "Y: ";
		if(!isDrawPosition || mouse_x_value == null){
			x_value += "none";
			y_value += "none";
//			setToolTipText("");
//			toolTip.show(false);
		}
		else{
			Date t = new Date(rm_x_value*1000);
			String st = t.toLocaleString();//toGMTString();
			x_value += st;//st.substring(st.indexOf(" "));
			y_value += rm_y_value;
//			setToolTipText(x_value+" "+y_value);
//			toolTip.show(true);
//			Graphics g2 = toolTip.getGraphics();
//			g2.setFont(font);
//			g2.drawString(x_value, 10, (int)(h+5));
//			g2.drawString(y_value, 10, (int)(2*h+5));
//			x_value +=
//			x_value +=mouse_x_value.toString();
		}
//		g.drawString(x_value, title_in_pixels+10, (int)(h+5));
//		g.drawString(y_value, title_in_pixels+10, (int)(h+5));
		//String st = "x:" + 
	}
	
	private boolean nearPoint(int x1,int y1){
		boolean answ = false;
		if(x1 > mouse_x_value-mouseMagneticValue && x1 < mouse_x_value+mouseMagneticValue && y1>mouse_y_value -mouseMagneticValue && y1<mouse_y_value+mouseMagneticValue)
			answ = true;
		return answ;
	}
	protected void paintComponent(Graphics g) {
//		font = g.getFont();
		//setToolTipText("test");
//		toolTip = createToolTip();
//		toolTip.setTipText("test");
//		add(toolTip);
//		toolTip.show();
		g.setFont(font);
		if(font.getSize() != 10){
			font = font.deriveFont((float)10.);
			g.setFont(font);
		}
//		titleFont = font.deriveFont((float)16.);
		if (g instanceof Graphics2D){
			((Graphics2D)g).setRenderingHint (RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);                         
			((Graphics2D)g).setRenderingHint (RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);                         
			((Graphics2D)g).setRenderingHint (RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);                         
//			((Graphics2D)g).setStroke( new java.awt.BasicStroke( 1.2f ) );
		}
		setNumberYValue(fr);
		setNumberXValue(fr);
//		g.fillRect(getX(), getY(), getWidth()-10, getHeight()-10);
//		AffineTransform at = null;
		int width = (int)(frame_max_x - frame_min_x);
		double height = frame_max_y - frame_min_y;
//		double scale = getWidth() / (width*1.);
		scalex = (float) ((getWidth()-indentXStart - indentXEnd) / (width*1.));
		if(height != 0)
			scaley = (float) ((getHeight() - indentYstart - indentYEnd) / (height));
		else
			scaley = Float.MIN_VALUE;
//		double scale;
//		if(scalex < scaley)
//			scale = scalex;
//		else
//			scale = scaley;
		if(min_y == 0 && max_y == 0){
			frame_max_y = 0;
			frame_min_y = 0;
			scaley = 0;
		}
//		at = new AffineTransform(scale,0,0,scale,getX(),getY());
		g.setColor(bgAllColor);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(bgGraphColor);
		g.fillRect(indentXStart, indentYEnd-indentUpToY, getWidth() - indentXEnd-indentXStart, getHeight()-indentYEnd-indentYstart+indentUpToY);
//		fillRect(g,0, 0, (int)((frame_max_x - frame_min_x)*scalex), (int)((max_y - min_y)*scaley));
		g.setColor(Color.black);
		drawTitle(g,5,5);
		if(number == 0){
			g.setColor(Color.black);
			g.drawLine(indentXStart, indentYEnd-indentUpToY, indentXStart, getHeight()-indentYstart);
			g.drawLine(indentXStart,getHeight()-indentYstart+indentUpToY,getWidth() - indentXEnd,getHeight()-indentYstart+indentUpToY);
			drawYAxisLabel(g,0,(int)(0),getFloatValue((float)0));
			return;
		}
		g.setColor(Color.black);
		drawAxis(g,scalex,scaley);
		//g.drawLine(0,0, 0,  - 10);

		isDrawPosition = false;
//		float srcPts[] = new float[4];
		float dstPts[] = new float[4];
		if(number == 1){
			g.setColor(Color.red);
			dstPts[0] = (float)((x[0]-frame_min_x) *scalex);
			dstPts[1] = y[0]*scaley;
			g.fillOval((int)dstPts[0]+indentXStart-1,(int)(getHeight()-indentYstart-dstPts[1]-1 +frame_min_y*scaley), 4, 4);
			if(mouse_x_value != null && mouse_y_value != null){
				if(nearPoint((int)dstPts[0],(int)(dstPts[1]))){
					rm_y_value = y[0];
					rm_x_value = x[0];
//					mouse_x_value = (int)dstPts[0];
//					mouse_y_value = (int)(dstPts[1]);
					isDrawPosition = true;
				}
			}
			
		}
		for(int i =1;i<number;i++){
//			srcPts[0] = (float)(x[i-1]-min_x);
//			srcPts[1] = y[i-1];
//			srcPts[2] = (float)(x[i]-min_x);
//			srcPts[3] = y[i];
//			at.transform(srcPts, 0, dstPts, 0, 2);
			dstPts[0] = (float)((x[i-1]-frame_min_x) *scalex);
			dstPts[1] = y[i-1]*scaley;
			dstPts[2] = (float)(x[i]-frame_min_x)*scalex;
			dstPts[3] = y[i]*scaley;
			
			if(height == 0){
				dstPts[1] = 0;
				dstPts[3] = 0;
			}
			
			
			g.setColor(lineColor);
			drawLine(g,(int)dstPts[0],(int)(dstPts[1]),(int)dstPts[2],(int)(dstPts[3]),true);
			g.setColor(Color.red);
//			if(i==1)
				g.fillOval((int)dstPts[0]+indentXStart-1,(int)(getHeight()-indentYstart-dstPts[1]-1 +frame_min_y*scaley), 4, 4);
			g.fillOval((int)dstPts[2]+indentXStart-1,(int)(getHeight()-indentYstart-dstPts[3] -1 +frame_min_y*scaley), 4, 4);
			if(mouse_x_value != null && mouse_y_value != null){
				if(nearPoint((int)dstPts[0],(int)(dstPts[1]))){
					rm_y_value = y[i-1];
					rm_x_value = x[i-1];
//					mouse_x_value = (int)dstPts[0];
//					mouse_y_value = (int)(dstPts[1]);
					isDrawPosition = true;
				}
				if(nearPoint((int)dstPts[2],(int)(dstPts[3]))){
					rm_y_value = y[i];
					rm_x_value = x[i];
//					mouse_x_value = (int)dstPts[2];
//					mouse_y_value = (int)(dstPts[3]);
					isDrawPosition = true;
				}
			}
//			g.drawLine((int)dstPts[0],(int)(getHeight() - dstPts[1]-1),(int)dstPts[2],(int)(getHeight()-dstPts[3]-1));
		}
		g.setColor(Color.black);
		//drawMouseValues(g);
//		at.
		//g.drawLine(0,0, 100, 100);
//		if(p != null)
//			g.drawLine(100, 100, (int)p.getX(), (int)p.getY());
	}
//	public void actionPerformed(ActionEvent e) {
//		Object src = e.getSource();
//		if(src == btnOptions){
//			OptionFrame frame = new OptionFrame();
//			frame.setSize(200, 200);
//			frame.show();
//		}
//		
//	}
	public void mouseDragged(MouseEvent arg0) {
		
	}
	public void mouseMoved(MouseEvent e) {
//		int x_value = e.getX();
//		int y_value = e.getY();
//		if(x_value >indentXStart && x_value<(getWidth()-indentXStart)&& y_value > indentYstart && y_value<(getHeight() - indentYstart)){
//			mouse_x_value = x_value-indentXStart;
//			mouse_y_value = getHeight()-y_value-indentYstart-indentUpToY+(int)(frame_min_y*scaley);
//			repaint();
//		}
//		else{
//			if(mouse_x_value != null){
//				mouse_x_value = null;
//				mouse_y_value = null;
//				repaint();
//			}
//		}
	}


}

