package wmi.tree;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

public class WMITreePanel extends JPanel{
	JTree tree = null;
	DefaultMutableTreeNode root = null;
	JScrollPane treeView = null;
	String textTree = null;
	WMITreePanel(){
//		root = new DefaultMutableTreeNode("root");
//		root.setAllowsChildren(true);
//		DefaultMutableTreeNode ch1 = addChildren(root,"test1");
//		addChildren(ch1,"test2");
//		addChildren(ch1,"test3");
//		addChildren(ch1,"test4");
//		addChildren(root,"test5");
//		root.add(new DefaultMutableTreeNode("test1"));
//		root.setAllowsChildren(true);
//		tree = new JTree(root);
//		setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
//		tree.setSize(getWidth(), getHeight());
//		treeView = new JScrollPane(tree);
//		treeView.setSize(getWidth(), getHeight());
////		setPreferredSize(new Dimension(getWidth(), getHeight()));
//		add(treeView);
	}
	private DefaultMutableTreeNode addChildren(DefaultMutableTreeNode parent,String name){
		DefaultMutableTreeNode child = new DefaultMutableTreeNode(name);
		parent.add(child);
		return child;
	}
	//first value index, second type of index
	private int[] getIndex(String tree_text){
		int ret_value[] = {-1,-1};
		int number1 = tree_text.indexOf("/<");
		int number2 = tree_text.indexOf("/>");
		int number3 = tree_text.indexOf("/]");
		if(number1 != -1){
			ret_value[0] = number1;
			ret_value[1] = 1;
		}
		if(number2 != -1 && (ret_value[0] == -1 ||ret_value[0]>number2)){
			ret_value[0] = number2;
			ret_value[1] = 2;
		}
		if(number3 != -1 && (ret_value[0] == -1 ||ret_value[0]>number3)){
			ret_value[0] = number3;
			ret_value[1] = 3;
		}
		return ret_value;
	}
	public boolean parce(){
		if(treeView != null){
			remove(treeView);
			treeView = null;
		}
		if(tree != null)
			tree = null;
		boolean finish = false;
		if(textTree == null)
			return false;
		int[] number = getIndex(textTree);
		if(number[0] < 1)
			return false;
		if(number[1] == 2 || number[1] == 3){
			finish = true;
		}
		
		String node_name = textTree.substring(0, number[0]);
		root = new DefaultMutableTreeNode(node_name);
		root.setAllowsChildren(true);
		String tree_text = textTree.substring(number[0]+2, textTree.length());
		parceElement(root,tree_text);
		tree = new JTree(root);
		setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
		tree.setSize(getWidth(), getHeight());
		treeView = new JScrollPane(tree);
		treeView.setSize(getWidth(), getHeight());
		add(treeView);
		return true;
	}
	
	private String parceElement(DefaultMutableTreeNode node,String tree_text){
		DefaultMutableTreeNode node_children = null;
		while(true){
			int[] number = getIndex(tree_text);
			if(number[0] == -1)
				break;
			if(number[0] > 0){
				String node_name = tree_text.substring(0, number[0]);
//				if(node == null){
//					root = new DefaultMutableTreeNode(node_name);
//					root.setAllowsChildren(true);
//					node = root;
//					//String tree_text = textTree.substring(number[0]+2, textTree.length());
//				}
//				else 
					node_children = addChildren(node,node_name);
			}
			tree_text = tree_text.substring(number[0]+2, tree_text.length());
				
			if(number[1] == 1){
//				if(node_children == null){
//					tree_text = parceElement(root,tree_text);
//				}
//				else
					tree_text = parceElement(node_children,tree_text);
			}
			else if(number[1] == 2){
				break;
			}
		}
		return tree_text;
	}
	
	protected void paintComponent(Graphics g) {
		tree.setSize(getWidth(), getHeight());
		treeView.setSize(getWidth(), getHeight());
		//tree.repaint();
	}
}

